medtextanalysis package
=======================

Subpackages
-----------

.. toctree::

    medtextanalysis.GUI
    medtextanalysis.algorithm
    medtextanalysis.core
    medtextanalysis.data_processing
    medtextanalysis.examples
    medtextanalysis.resources
    medtextanalysis.sentence2report
    medtextanalysis.tests
    medtextanalysis.util

Submodules
----------

medtextanalysis\.MTA module
---------------------------

.. automodule:: medtextanalysis.MTA
    :members:
    :undoc-members:
    :show-inheritance:

medtextanalysis\.config module
------------------------------

.. automodule:: medtextanalysis.config
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: medtextanalysis
    :members:
    :undoc-members:
    :show-inheritance:
