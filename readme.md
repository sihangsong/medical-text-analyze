# medtextanalysis

医疗文本分析器

介绍
---

这是一个private的项目，目的是对于病例报告中特征的提取和处理

### 使用方法

这是基于CookieCutter模板生成的项目 [cookiecutter template](https://github.com/mandeep/cookiecutter-pyqt5). 

#### 如何安装

**首先这是一个python3项目，基于python3.5+以上环境开发**

最简单的方法是使用pip(建议使用virtualenv新建环境)
```python
pip install -r requirements.txt
```
除此以外目前项目中内嵌了部分PyQt5的代码，这个包只能手动安装，但是不安装不影响
其他功能，暂时忽略它就好。如果python-crfsuite装不上，你可能需要Anaconda
然后使用conda安装，在安装python-crfsuite的时候得使用命令:

```python
conda install -c conda-forge python-crfsuite
```

#### 将文件安装到python环境中
你也可以直接让python帮你装到当前的python环境中，然后作为一个库
（名字就叫 medtextanalysis）来使用
```python
python setup.py install
```

#### 如何运行

就像你见过的任何一个python包一样

```python
from medtextanalysis.examples.demo_crfsuite import test_all_test_samples
from medtextanalysis.algorithm.tagger import CRFTagger
from medtextanalysis.algorithm.trainer import CRFTrainer

# Some code blablabla...
...

test_all_test_samples(inputfile, outputfile, CRFTagger)

```

或者你可以直接运行代码下面examples文件夹里的脚本

#### 单元测试(仅供内部开发)

```python
pytest -s medtextanalysis
```

#### pylint检查
Note PyQt is not in libs, that's an extension. You need to
load it manually via

```
pylint --extension-pkg-whitelist=PyQt5 medtextanalysis
```


### 临时记录:
*   标记的时候如果原则上词语不能有空格， 如果实在需要用空格隔开，可以
在空格部分标注 "_" 以声明这里有个空格。
*   括号内标记或者周围有特殊字符的时候，标记记得隔开他们，以下标记都是不太合法的标记：
    *   (三@q 天) (会被误认为 (三 是个词  )
    *   SO2@nz↓ nz和箭头在一起不会让程序识别部出nz，而是可能识别不出箭头


