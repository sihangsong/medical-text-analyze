from setuptools import setup

requirements = [
    'python-crfsuite',
    'pandas',
    #'gensim',
    #'jieba==0.39',
    'scikit-learn==0.19.0',
    'xlrd',
    'xlwt',
    'XlsxWriter==0.9.8',
    #'sphinx',
    #'sphinx_rtd_theme',
    'numpy',
    'matplotlib',
    'thrift'
]

test_requirements = [
    'pytest',
    'pytest-cov',
    'pytest-faulthandler',
    'pytest-mock',
    'pytest-qt',
]


setup(
    name='medical_text_analyzer',
    version='0.1.0',
    description="A Chinese medical treatment analyzer",
    author="panzer_wy,ssh",
    author_email='panzer.wy@gmail.com',
    #url='https://github.com/panzer_wy/medical_text_analyzer',
    packages=['medtextanalysis', 'medtextanalysis.core', 'medtextanalysis.data_processing',
              'medtextanalysis.examples','medtextanalysis.algorithm',
              'medtextanalysis.tests','medtextanalysis.tests.data_processing',
              'medtextanalysis.protocol','medtextanalysis.GUI',
              'medtextanalysis.util','medtextanalysis.core.IO'],
    package_data={
        'medtextanalysis.resource.images': ['*.png'],
        'medtextanalysis.resource' : ['*.xls','*.txt'],
        'medtextanalysis.examples' : ['readme.md']
    },
    entry_points={
        'console_scripts': [
            'MTA_train_tool=medtextanalysis.MTA_train_tool:run_train_tool',
            'MTA_server=medtextanalysis.MTA_server:run_server'
        ]
    },
    install_requires=requirements,
    zip_safe=False,
    keywords='medical_text_analyzer',
    classifiers=[
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    tests_require=test_requirements,
)
