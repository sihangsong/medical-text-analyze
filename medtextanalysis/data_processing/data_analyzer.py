# -*- coding: utf-8 -*-

import pandas as pd

import medtextanalysis.examples.label_validation as va
import sentence2report.sentence_split as ss

stop_word_list= u'[,，\s、　\'’\n。\;；：【】（）]+'

#TODO: 重构该代码

def write_sample_line(f,phrase_list):
    for each in phrase_list:
        if isinstance(each,str):
            if len(each) > 0:
                f.write(each)
        else:
            f.write(str(each))
        f.write(' ')
    f.write('\n')


def check_sample_line(phrase_list):
    checked_one= []
    for each in phrase_list:
        if isinstance(each,str):
            if len(each) > 0:
                checked_one.append(each)
        else:
            checked_one.append(str(each))
    return checked_one


def labeltonum(label):
    """
    标记测试数据
    :param label:
    :return:
    """
    if label in ['nz','nr','eng']:
        return 1
    elif label in ['a','d','item','q']:
        return 2
    else:
        return 0


def get_train_test_pack(input_file):
    """
    Instead of
    :param input_file:
    :return:
    """
    sample = pd.read_excel(input_file)
    index = 1
    train_labels=[]
    train_words=[]
    test_labels=[]
    test_words=[]
    print('Reading from raw file! (With labeled data)')
    for each in sample['内容']:
        if not isinstance(each, str):
            print("Error at reading line %s" % str(index))
            continue
        split_phrases= [each]
        for each_phrase in split_phrases:
            if len(each_phrase) == 0:
                continue
            ## lazy way to split
            ## TODO: change this one
            if index <= 500:
                str_args, label_args = va.extract_input(each_phrase)
                train_labels+=[label_args]
                train_words+=[str_args]
            else:
                str_args, label_args = ss.feature_extraction(each_phrase)
                test_labels += [[labeltonum(label) for label in label_args]]
                test_words += [str_args]
        index = index + 1
    return train_words,train_labels,test_words,test_labels

