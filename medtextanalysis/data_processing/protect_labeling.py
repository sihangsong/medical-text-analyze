"""
Update on Dec, 2017

用于根据保护字典做一次全局搜索
"""

__author__ = "panzer.wy"

def _add_one_label_for_word(document,word,label):
    cut_list = document.split(word)
    str_list = []
    for item in cut_list[:-1]:
        str_list.append(item)
        str_list.append("".join([" ",word,"@",label," "]))

    str_list.append(cut_list[-1])
    return "".join(str_list)

def add_label_by_protect_dict(document,protect_dict):
    """

    :param document:
    :param protect_dict:
    :return:
    """
    for each in protect_dict.keys():
        document = _add_one_label_for_word(document,each,protect_dict[each])
    return document

