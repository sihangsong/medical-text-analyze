
'''
Write Dictionary files based on the labeled data
'''

import codecs

import pandas as pd

from medtextanalysis.examples.label_validation import check_input


#TODO: 重构该代码
def generate_dict(input_file):
    sample = pd.read_excel(input_file)
    index = 1
    #print('Reading from raw file! (With labeled data)')
    count_list=[]
    for each in sample['内容']:
        if not isinstance(each, str):
            print("Error at line %s" % str(index))
            continue
        split_phrases = [each]
        for each_phrase in split_phrases:
            if len(each_phrase) == 0:
                continue
            str_args, count_item = check_input(each_phrase,"red","blue")
            count_list+=count_item
        index+=1

    from collections import Counter
    w = Counter(map(tuple, count_list)).items()
    w = sorted(w, key=lambda a: a[1])
    result = []
    for each in w:
        result.append((each[0][0], each[1], each[0][1]))
    with codecs.open('dict_4.0.txt', 'w', "UTF-8") as f:
        for each in result:
            # throw low frequency word
            if len(each[0]) == 0 :
                continue
            f.write(each[0])
            f.write(' ')
            #f.write(str(each[1]))
            #f.write(' ')
            f.write(each[2])
            f.write('\n')
    #print('done!')
