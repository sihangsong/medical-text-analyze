"""
数据清洗脚本:这里是一个从输入数据字符串到字符串/字符串列表的系列脚本的集合，主要目的是把数据处理成
便于序列标注的形式

主要难点在于：

1. 数据格式非常混乱
2. 得把基于规则可以提取的部分和下一步基于学习的部分分离开，基于规则提取的东西虽然也可以被学到，但是
这里由于1.的存在(即数据格式较乱)，它会严重影响对中文的学习效果。
3. 可扩展性比较麻烦

实现接口：(均假设读入原始字符串，且输出不是字符串，就是字符串的列表)

1. 清除标点，全部以空格替换
2. 按照指定分隔符分割文本(可选择是否保留分隔符)
3. 标记保护特定正则匹配的内容（含英文的术语，单位，日期，科学计数法等）
4. 去除字符串中的@+词性的结构（把标记数据转换成未标记数据）
5. 标点特判，主要是将句子中的 '.','/','-','%',':' 等一系列特殊字符的作用分清楚。
    以'.'举例，如果这个'.'之前跟了一个汉字，那么几乎可以肯定这是"。"，但是如果之前和之后都跟了数字，
    那么几乎可以肯定这是小数点。如果是什么标号(例如:"2.") 这个东西的存在就非常麻烦，而且这属于小概率事件，
    也没必要通过标记学习。
6. 所以交互界面还是很有必要的，一方面降低标记数据难度，另一方面通过修正提高准确率。

建议执行顺序：
字符转换->特殊字符消岐->正则保护->分割句子->分割短语->生成标记/分词数据->还原正则

这里就没必要写类了，写成function就好了。


"""
import re

import medtextanalysis.core.error as error
from medtextanalysis.config import GlobalVar
from medtextanalysis.util.funcs import strQ2B


def _is_input_string(data):
    """
    检测下面的函数输入是否是字符串，不是则应该打印错误警告
    :param data:
    :return:
    """
    if not isinstance(data, str):
        try:
            raise error.TypeError(type(data), "str")
        except error.TypeError as e:
            print(e.err_msg)
            return False
    return True


def _is_input_all_chinese(data):
    """
    检测输入字符串是否全是汉字
    :param data:
    :return:
    """
    for ch in data:
        if not (u'\u4e00' <= ch <= u'\u9fff'):
            return False
    return True


def _is_input_contain_chinese(data):
    """
    检测输入字符串是否含有汉字
    :param data:
    :return:
    """
    for ch in data:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
    return False


def unify_special_symbols(document):
    """
    这个方法专门处理输入中一些具有二义性的字符，要么把它们替换成一个没有歧义的符号，
    要么把它们从句子中删掉。

    现在考虑的情况：
        把前面跟汉字的'.'换成'。'
        把前面跟汉字的":"替换成 ","
        把所有全角字符转换成半角字符
        把所有符号统一(参考config.py的设置)

    :param document:
    :return:
    """

    def check_dot(prev, now):
        """
        检查当前字符和它的上一个有效字符(非空格的第一个字符)，并决定返回内容
        :param prev:
        :param now:
        :return:
        """
        if now is not ".":
            return now

        if not _is_input_string(prev):
            return "."

        # 这是句号
        if _is_input_all_chinese(prev):
            return u"。"
        # 这是小数点
        if prev.isdigit():
            return u"."
        if 'a' <= prev <= 'z' or 'A' <= prev <= 'Z':
            return u"."
        # 前面不是汉字的时候，假设是逗号好了
        return u","

    def check_colon(prev,now):
        if now is not ":" or now is not "：":
            return now
        if _is_input_all_chinese(prev):
            return ","
        return ":"

    def check_divide(prev, now):
        if now is not "/":
            return now
        if prev in [u"+",u"-",u"＋",u"－"]:
            return "|"
        return "/"

    def check_right_parenthesis(prev, now):
        if now is not ")":
            return now
        if prev == u"." or '0' <= prev <= '9':
            return ""
        print(prev)
        return ")"

    prev_chr = ''
    document= document.lstrip(" ").rstrip(" ")
    document= strQ2B(document)
    new_document = []

    # 没啥好办法，循环吧
    for each in document:
        # 没啥好办法，一个个判断吧

        # 首先全角转半角
        num = ord(each)
        if num == 0x3000:
            num = 32
        elif 0xFF01 <= num <= 0xFF5E:
            num -= 0xfee0
        each = chr(num)

        # 然后开始处理具有二义性的字符:
        each = check_dot(prev_chr, each)
        each = check_colon(prev_chr, each)
        each = check_divide(prev_chr, each)

        # 标点统一化
        if each in GlobalVar.REPLACE_WORD_DICT.keys():
            each = GlobalVar.REPLACE_WORD_DICT[each]

        if each is not " ":
            prev_chr = each
        new_document.append(each)

    if new_document[-1]==".":
        new_document[-1]="。"
    # 返回结果
    return "".join(new_document)


def remove_separator(document,
                     separator=None,
                     replace_chr=" "):
    """
    将所有分隔符替换为 ' '后返回处理后的字符串。如果不指定，缺省为全局默认值

    注意该方法不检查分隔符的语境，我们假设这个文档没有疯狂到使用 '/', '-'等易混字符来分割上下文
    :param document:
    :param separator:
    :param replace_chr:
    :return:
    """
    if not _is_input_string(document):
        return ""
    if separator is None:
        separator = GlobalVar.SEPARATE_WORD_LIST

    regex_pattern = "".join(["["] + separator + ["]+"])

    return replace_by_regex(document,
                            regex_pattern,
                            replace_chr,
                            remain_match_groups=False)


def split_by_symbol(document,
                    separator=None,
                    remain_separator=False):
    """
    按照分隔符断句并返回str list
    如果要求保留分隔符，则额外返回一个list表示分割内容，长度应该比分割后的list长度恰好小1
    (注：之所以默认不插进去是因为我认为大部分情况下不需要这些标点，所以分开返回了)。

    :param document:
    :param separator:
    :param remain_separator:
    :return:
    """
    if not _is_input_string(document):
        return ""
    if separator is None:
        separator = GlobalVar.STOP_WORD_LIST

    regex_pattern = "".join(["["] + separator + ["]{1}"])

    return split_by_regex(document,
                          regex_pattern,
                          remain_match_groups=remain_separator)


def remove_data_labels(document,label_regex_pattern=u"@[A-za-z]*"):
    """
    去除匹配到的所有标记数据样式并返回未标记的字符串
    我们假定标记样式服从正则匹配规则且不会出现错误。

    :param document:
    :param label_regex_pattern:
    :return:
    """
    if not _is_input_string(document):
        return ""
    return replace_by_regex(document,
                            label_regex_pattern,
                            "",
                            remain_match_groups=False)


def replace_by_regex(document,
                     pattern_str,
                     replace_str,
                     pattern_name="something",
                     remain_match_groups=True,
                     indexing=False
                     ):
    """
    将匹配的正则规则替换为指定类型的字符串,并返回替换后的结果.
    这里假定正则是不分组的
    如果选择保留查询到的所有结果，则会返回两个值
    主要作用：基于规则的同义词替换，脱去训练标签

    :param document:
    :param pattern_str:
    :param replace_str:
    :param pattern_name:
    :param remain_match_groups:
    :param indexing
    :return:
    """
    if not _is_input_string(document):
        return ""

    regex_pattern= re.compile(pattern_str)
    match_contents = regex_pattern.findall(document)

    if indexing is True:
        count = -1

        # 定义一个函数添加标号
        # 这里利用了python3的nonlocal简化了代码(不然得写循环了)
        def sub_func(match):
            nonlocal count
            count += 1
            return replace_str + "[%s]" % str(count)
        document = regex_pattern.sub(sub_func, document)
    else:
        document = regex_pattern.sub(replace_str, document)

    if remain_match_groups is True:
        return document, match_contents
    else:
        return document


def split_by_regex(document,
                   pattern_str,
                   pattern_name="something",
                   remain_match_groups=True,
                   ):
    """
    按照正则规则断句并返回分割后的结果和正则提取的数据列表。
    可以选择保留正则内容在分割结果中，这样正则内容也会作为一个单元(把每个内容看成单字)放在里面。

    :param document:
    :param pattern_str:
    :param pattern_name:
    :param remain_match_groups:
    :return:
    """
    if not _is_input_string(document):
        return ""

    regex_pattern= re.compile(pattern_str)

    split_result = regex_pattern.split(document)
    match_contents = regex_pattern.findall(document)

    # 处理成等长序列
    # 千万要小心，不然很多基于等长列表的操作就会失效
    if len(split_result) < len(match_contents):
        split_result.extend([" "] * (len(match_contents) - len(split_result)))
    elif len(split_result) > len(match_contents):
        match_contents.extend([" "] * (len(split_result) - len(match_contents)))

    # Python 3 黑魔法之yield 和zip
    # 可以处理等长的交错合并
    def interpolate(*seqs):
        for items in zip(*seqs):
            yield from items

    if remain_match_groups is True:
        # 这里的split_result是Iterator!千万小心 list(split_result)才是列表
        if len(match_contents) > 0:
            split_result = interpolate(split_result,match_contents)
    combined_result = []

    # Iterator 是有 __iter__ 方法的
    for each in split_result:
        if len(each) > 0 and each!=" ":
            combined_result.append(each)
    return combined_result, match_contents

# 优雅地去掉空格
# 句子开头，标点前后的空格直接去掉
# 而若干个空格合并成','
def remove_white_space(document):
    """
    把分割用的空格换成逗号
    :param document:
    :return:
    """
    document = document.lstrip(" ").rstrip(" ")

    def _is_eng(chr):
        return "a" <= chr <= "z" or "A" <= chr <= "Z"
    new_document =[]
    for i in range(len(document)):
        if i == 0 or i == len(document)-1:
            continue
        prev = document[i-1]
        now = document[i]
        next = document[i+1]
        if now == " " and _is_eng(prev) and _is_eng(next):
            new_document.append(",")
        else:
            new_document.append(document[i])
    return "".join(new_document)

# 还原正则
def recover_from_regex(phrases, word_list):
    """
    把正则换成表里的东西
    :param phrases: 输入的短语序列(list)
    :param word_list: 所有正则的字典
    :return:
    """
    replaced_phrases = []

    # 这个语法是闭环: {name}[num] 这个标记方式不对用户开放
    # 所以这样写没问题
    _number_catch_reg = "{([A-Za-z_]+)}\[(\d+)\]"
    pattern = re.compile(_number_catch_reg)

    def replace_it(match):
        nonlocal word_list
        if len(word_list[match.group(1)])<=int(match.group(2)):
            print("Error!")
        return word_list[match.group(1)][int(match.group(2))]

    for each in phrases:
        replaced_phrases.append(pattern.sub(replace_it, each))
    return replaced_phrases
