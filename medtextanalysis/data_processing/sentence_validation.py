# -*- coding: utf-8 -*-
"""
病例数据分析
10.9
1.增加了错误信息的输出
    现在的输出为：正确的二元组统计、词性统计、错误二元组统计
2.优化了错误的识别
    优化了content部分开头出现词性的情况
    现在还有的错误包括：content前后有引号/括号；content开头出现无意义数字/日期。
    做更多优化的话要大致对结果进行一次大致的清洗
3.后续可以加入的功能？？
    error_locate：错误的定位（这次没来得及做）
    add：增加文本数据，自动更新词典
    show：以图表形式展示统计结果
"""

import re
from collections import Counter

import pandas as pd
import xlrd


class nlp:
    """在定义nlp类的时候给出文件地址，nlp会对地址文件自动进行处理

    varilables：
    dict：统计结果的（内容，词性）二元组字典
    content_d：内容的字典
    property_d：词性的字典
    triple：（内容，词性，频数）三元组

    functions：
    analyze函数可以统计错误的结果
    show函数可以进行字典打印
    output函数可以输出统计结果
    其他工具函数及函数详情见函数内注释
    """
    source = ''
    string = ''   #source读出的字符串信息
    result = []   #结果的二元组列表
    dict = {}     #结果的字典
#    content= []  #内容的字符串列表
    content_d = {}#内容的字典
#    property= [] #词性的字符串列表
    property_d = {}#词性的字典
    triple = []
    error={}
    def __init__(self, address, col):
        """运用init实现文件读入
        adress:文件地址
        col：excel中数据信息 所在列数-1（即首列为0）
        """
        self.source = address
        self.string = self.__read(col)
        self.result = self.__save(self.string)
        self.dict = Counter(self.result)
        self.error=self.__analyze(self.dict)
        for item in self.error.keys(): 
            self.dict.pop(item)
            for i in range(self.result.count(item)):
                self.result.remove(item)
        self.content_d = Counter(map(lambda a: a[0], self.result))
        self.property_d = Counter(map(lambda a: a[1], self.result))
        #注意 map在python3.0为map类，迭代之后为空,而python 2.6 直接转换为列表
        self.triple = list(map(lambda a: (a[0][0], a[0][1], a[1]), self.dict.items()))
        

    def __judge(self, character):
        """判断character是不是分隔符
        return：是1否0
        """
        if character in ['.', ';', '；', '\'', ' ', ',', '，', '：', ':', '。', '）',')','(', '（', '、', '\\', '×']:
            return 1
        else:
            return 0


    def __check0(self, string):
        """识别词性英文短语
        return：是1否0
        """
        match = re.search(r'^ad|an|az|a|cO|cc|c|d|f|nr|ns|nt|nz|n|p|q|r|t|udeng|vyou|v|z',string,re.I)
        if match:
            return len(match.group())
        else:
            return 0

    def __locate(self, sentence, i, last_end):
        """给出指定位置相邻分隔符的位置
        sentence:字符串
        i:@符号位置
        last_end：上一个词性结束的位置
        return：（内容开头位置，@位置，词性结束位置）三元组
        """
        flag = i
        tmp = i
        end = i+self.__check0(sentence[i:(i+6)])
        if sentence[tmp-1] == '）'or sentence[tmp-1] == ')':  #用来鉴别（-）与（+）
            while tmp > 0:
                tmp = tmp-1
                if sentence[tmp] == '（' or sentence[tmp] == '(':
                    beg = tmp
                    break
        else:
            beg = last_end+1
            while tmp > last_end:
                tmp = tmp-1
                if self.__judge(sentence[tmp]):
                    beg = tmp+1
                    break
        return(beg, flag, end)

    def __cut(self, sen, tup):
        """由位置三元组定位，截取字符串
        sen:字符串
        tup:表示位置的数字三元组
        return：由三元组截取字符串的两端，组成二元组
        """
        return(sen[tup[0]:tup[1]], sen[(tup[1]+1):(tup[2]+1)])


    def __save(self, string):
        """对string进行处理，统计（内容，词性）二元组
        return:（内容，词性）的二元组列表
        """
        tmp = []
        last_end = 0
        for i, item in enumerate(string):
            if item == '@':
                haha = self.__locate(string, i, last_end)
                tmp.append(haha)
                last_end = haha[2]
        stat = []
        for i, item in enumerate(tmp):
            stat.append(self.__cut(string, item))
        return stat

    def __check(self, string):
        """对中文，非字母非数字的符号进行识别
        return：是1否0
        """
        pattern = re.compile(r'[\u4e00-\u9fa5\W]+')
        match = pattern.search(string)
        if match:
            return 1
        else:
            return 0

    def __read(self, col):
        """读入数据,以字符串的形式返回
        col:excel中数据信息 所在列数-1（即首列为0）
        return:字符串化的文本
        """
        table = xlrd.open_workbook(self.source).sheets()[0]
        #nrows = table.nrows
        #table.row_values(2) 查看某行的值
        yiliao = table.col_values(col)
#==============================================================================
#         for i in range(nrows):
#             yiliao.append(table.row_values(i))
#             yiliao_n.append(len(str(yiliao[i])))
#         for i in range(nrows):
#             yiliao[i]=str(yiliao[i])
#==============================================================================
        return str(yiliao)

    def __analyze(self, dict0):
        """返回部分错误内容、词性
        dict0:所要分析的字典
        print：某些错误的二元组
        """
        error={}
        for item in list(dict0.keys()):
            if self.__check(item[1]) or item[1] == '':
                error[item]=dict0[item]
            elif item[0].count('@') or item[0] == '':
                error[item]=dict0[item]
            match = re.match(r'^n|u3000', item[0])   #这一步是识别content开头错误识别出的词性，经人工检查后发现主要是content开头多一个n，就改成只检查n了
            if match:
                error[item]=dict0[item]
        return(error)

#==============================================================================
#     def show(self, opt):
#         """打印字典
#         这个功能好像有点鸡肋，因为直接写文件名就可以打印词典，不过还是留在这里算了
#         opt:1打印字典 2仅内容 3仅词性
#         """
#         if opt == 1:
#             for item in list(self.dict.keys()):
#                 print(item, ':', self.dict[item])
#         if opt == 2:
#             for item in list(self.content_d.keys()):
#                 print(item, ':', self.content_d[item])
#         if opt == 3:
#             for item in list(self.property_d.keys()):
#                 print(item, ':', self.property_d[item])
#==============================================================================

    def output(self, address):
        """将内容词性的统计输出到地址，存为excel文件
        adress:输出地址
        """
        writer = pd.ExcelWriter(address)
        pd.DataFrame(self.triple).to_excel(writer, 'Content', header=False, index=False)
        pd.DataFrame(self.property_d, ['Frequency']).T.to_excel(writer, 'Property')
        pd.DataFrame(self.error, ['Frequency']).T.to_excel(writer, 'Error')
        writer.save()


#==============================================================================
##pylint用法
# import pylint,os
# os.system('cmd')
# pylint hank.py
#==============================================================================
