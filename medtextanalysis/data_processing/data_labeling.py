"""
处理和标记数据相关的问题,应该包括训练集和测试集的标注


"""
import re

import medtextanalysis.core.error as error
from medtextanalysis.config import GlobalVar


def _is_input_string(data):
    """
    检测下面的函数输入是否是字符串，不是则应该打印错误警告
    :param data:
    :return:
    """
    if not isinstance(data, str):
        try:
            raise error.TypeError(type(data), "str")
        except error.TypeError as e:
            print(e.err_msg)
            return False
    return True

def _is_input_all_english(data):
    """
    检测是否是一个英文单词
    :param data:
    :return:
    """
    if not _is_input_string(data):
        return False

    # 假定输入已经是个字符串了
    for each in data:
        if 'a' <= each <= 'z' or 'A' <= each <= 'Z':
            pass
        else:
            return False
    return True


def get_protected_regex_label(component_list, old_label_list, regex_pattern=u"{([A-Za-z_]+)}\[(\d+)\]"):
    """
    把正则保护的数据的标签提取出来，标签就是它们的名字，
    这个可以解决一部分unknown的标注
    :param component_list:
    :param old_label_list:
    :param regex_pattern:
    :return:
    """
    if not isinstance(component_list, list):
        try:
            raise error.TypeError(type(component_list),"list")
        except error.TypeError as e:
            print(e.err_msg)
            return [],[]

    regex_pattern = re.compile(regex_pattern)
    label_list=[]
    for i in range(len(component_list)):
        match = regex_pattern.match(component_list[i])
        if match:
            label_list.append(match.group(1))
        else:
            label_list.append(old_label_list[i])

    return label_list





def extract_train_data_and_labels(component_list,
                                  label_regex_group_pattern=u"([^\s]+)@{1}([A-Za-z]+)",
                                  ):
    """
    将列表中每个成分的标签后的标注词性提取出来
    :param component_list: 应该是列表 (目前没有实现单个的版本，因为觉得没有必要)
    :param label_regex_group_pattern:   默认一个字段只有一个标注数据，如果要松散的匹配请去掉
                                        该变量的"^"和"$"
    :return: 两个列表，一个是脱去词性标注后的结果，一个是词性，没标注的用标签"unknown"表示
    """
    if not isinstance(component_list, list):
        try:
            raise error.TypeError(type(component_list),"list")
        except error.TypeError as e:
            print(e.err_msg)
            return [],[]
    regex_pattern = re.compile(label_regex_group_pattern)

    word_list =[]
    label_list =[]

    def _split_one_label(string, pattern):
        """
        专门处理单个短语的分割
        :param string:
        :param pattern:
        :return:
        """

        # 先检测是不是字符串
        if not _is_input_string(string):
            return "", "unknown"
        # 看看是不是特殊字符
        if string in GlobalVar.ALL_SPECIAL_SYMBOL_LIST:
            return string, get_chr_type(string)

        # 否则，我们用正则提取标签
        match = pattern.match(string)
        if match:
            # 返回匹配结果
            return match.group(1), match.group(2)
        else:
            # 看看是不是没标记的英文单词
            # 之所以不用正则匹配纯英文单词，是怕重复匹配
            if _is_input_all_english(string):
                return string, "eng"

            # 再看看是不是特殊的东西:
            if string in ["天","次","分钟"]:
                return string, "unit"

            if string in ["周一","周二","周三","周四","周五","周六","周日"]:
                return string, "time"

            # 不知道，返回unknown标签
            return string, "unknown"

    for each in component_list:
        word, label = _split_one_label(each,regex_pattern)
        word_list.append(word)
        label_list.append(label)
    return word_list,label_list


def get_chr_type(chr):
    """
    得到一个char字符的类型标注
    :param chr:
    :return:
    """

    special_type_dict = GlobalVar.SYMBOL_LABEL_DICT

    if u'\u4e00' <= chr <= u'\u9fff':
        # 中文
        return "cn"
    elif 'a' <= chr <= 'z' or 'A' <= chr <= 'Z':
        # 英文
        return "eng"
    elif '0' <= chr <= '9':
        # 数字
        return "num"
    elif special_type_dict.get(chr,None) is not None:
        # 有特殊意义的符号
        return special_type_dict[chr]
    elif chr in GlobalVar.ALL_SPECIAL_SYMBOL_LIST:
        # 一般的符号
        return "symbol"
    else:
        return "unknown"
