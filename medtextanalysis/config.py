"""
这里是项目的全局设置

全局设置应该由一个全局变量字典，若干方法和类组成，字典和每个类内部的公有成员和方法，通过import在内部传递

这样一来可以防止引用混乱，二来可以通过这个类给外部留有修改的接口

该python程序包中的util模块将和这个设置进行通信，一些全局函数和全局变量可以转移到util模块中以
增强该文件的可读性。一开始就先都写在这里好了

注意系统常量默认全大写

"""

# TODO: (wy)完成config模块

class Config(object):
    """
    全局配置的基类
        专门用于和用户对接，得到一些自定义的参数变量，这些变量会随着用户的要求而改变
        这里给出这些变量的默认值

    """
    index = 0


    # 这是最简单的统计我们实例化了多少个类的方法
    @classmethod
    def count_index(cls):
        cls.index += 1

    def __init__(self,_id="",rootdir= None):
        """
        初始化
        """
        # 因为不知道绝对路径，所以只能用相对路径实现
        # 用于存放程序生成临时变量的根目录位置
        assert isinstance(_id, str)

        import os.path as ospath
        if rootdir is None:
            rootdir = ospath.abspath(".")
        self.temp_file_dir = ospath.join(rootdir, "resources/_cache")
        self.temp_crf_model_filename = ospath.join(self.temp_file_dir, "model" + _id + ".crfsuite")
        self.temp_log_filename = ospath.join(self.temp_file_dir, "client" + _id + ".log")
        self.merge_dict_stream = None
        self.search_dict_stream = None
        self.protect_dict_stream = None

        self.count_index()



class GlobalVar(object):
    """
    全局变量类
        专门用于生成全局配置所需要的变量，这些变量一般是不改变的
    """
    # 所有我见过的字符
    ALL_SPECIAL_SYMBOL_LIST = ["?", "Ⅳ", ":", "”", "↓", ";", "℃", "β", "-", "а",
                               "→", "(", "%", "〉", "~", "*", "\"", "‘", "’", ">", "。",
                               "】", "‰", "]", "【", "<", "×", ".", "!", "=", "^", "±",
                               ",", "`", "/", "\n", "+", "Ⅴ", "—", "、", "_", ")", "|",
                               "·", "#", "[", "↑", "α", "Ⅲ", "Ⅱ", "Ⅰ", "“"]

    # 停止字符：遇到该字符应该断句，且断句后把它们从句子中删掉不会影响表意
    STOP_WORD_LIST = [u"；", u"\;", u"。",  u"!", u"！", u"?", u"？"]
    # 分割字符：遇到该字符短语或单词必须结束，但是不断句
    SEPARATE_WORD_LIST = [u"，", u",", u" ", u"、", u":", u"：", u"\s"]
    # 无用字符：删去该字符对我们的任务没有任何影响，主要是成对的括号
    USELESS_WORD_LIST = [u"\\n", u"\(",u"\)",u"（",u"）",u"【",u"】",u"\'",u"\"",u"“",
                     u"”",u"‘",u"’",u"《",u"》",u"{",u"}",u"\[",u"\]"]
    # 这里发现标记中有括号的情况会造成麻烦，两个版本可以换着用试试看
    #USELESS_WORD_LIST = [u"\\n", u"【", u"】", u"\'", u"\"", u"“",
    #                            u"”",u"‘",u"’",u"《",u"》",u"{",u"}",u"\[",u"\]"]
    # 在这里有一类字符非常危险：它们既不起分割作用，也不起断句作用，但是你不能断定它们的出现一定是没用的。
    # 这个时候必须检查上下文，举例: 1)(无用字符)  APTX-4869(这个是连接符) 12-21日(日期) 250mg/mol(单位)

    # 上下文字符：功能性字符，遇到功能性字符需要格外注意。
    # CONTEXT_WORD_LIST = [u"/",u"%",u"-",u"_",u".",u"*"]

    # 允许的词性列表
    CIXING_LIST = [u"n",u"nz",u"nr",u"ns",u"nt",u"a",u"an",u"ad",u"az",
                   u"cO",u"cc",u"c",u"d",u"f",u"p",u"q",u"r",u"t",u"udeng",
                   u"v",u"vn",u"vyou",u"t",u"q",
                   #注意以下为标点和内置定义
                   u"stop",u"item",u"date",u"time",u"unit",u"num",u"eng",u"complex_eng",
                   u"comma",u"colon",u"connect",u"unknown"]

    # 替换列表
    REPLACE_WORD_DICT= {
        u"，": ",",
        u"、": ",",
        u"；": ",",
        u";": ",",
        u"…": " ",
        u"!": "。",
        u"！": "。",
        u"?": "。",
        u"？": "。",
        u"×": "*",
        u"（": "(",
        u"）": ")"
    }

    # 标点的标签列表:
    SYMBOL_LABEL_DICT = {
        u",": "comma",
        u"，": "comma",
        u":": "colon",
        u";": "semicolon",
        u"。": "stop",
        u"、": "comma",
        u"：": "colon",
        u"_": "connect",
        # 以下四个单独出现的时候是形容词
        u"+": "plus",
        u"-": "minus",
        u"↓": "down",
        u"↑": "up"

    }

    # 中文单位
    # 需要在标记的时候特判，记为单位
    # 需要在生成报告的时候找一下前面是否跟了中文数字，合并成item
    SPECIAL_CHINESE_UNIT = [u"天",u"次"]

    # 预定义正则

    # 用于探测科学记数法和常规数值，可以寻找: 12.34*10^10 , 235, -1.234*10^-10 这样的数字，可以处理绝大部分的情况
    _number_reg_str = u"(?:(?<![\d\[])(?:(?:\d+(?:\.\d+)?－)|[\<\>\-\+])?\d+(?:\.\d+)?(?:(?:[\*×](?:10)\^(?:-?(?!0\d)\d+)))?(?![\d\]]))"

    # 用于寻找单位，默认单位是 A/B 类型的，但是实际情况中单位有 A/B/C 类型的
    _unit_reg_str = u"(?:(?:(?<=[\d\s])[a-zA-z]*(?:(?:/[a-zA-z]+" \
                    u"(?![a-zA-z]))|[a-zA-z]+))|(?:(?<=[\d\s])\%{1})|(?:(?<=[\d\s])℃))"

    # 弱化的单位，广义上会把汉字等一些特殊的东西算作单位，一旦字段中有/就会容易弄混
    _weak_unit_reg_str = u"(?:(?![0-9]+)(?:(?<=[\d\s])[a-zA-z\u4e00-\u9fa5]*/(?:[^\[\]\s,，.。;；\:、@\+\-]+)" \
                         u"(?:/(?:[^\[\]\s,，.。;；\:]+))?)" \
                         u"|(?:(?![0-9\[\]]+)(?:(?<=[\d\s])[a-zA-z\u4e00-\u9fa5]+))" \
                         u"|(?:(?<=[\d\s])\%{1})|(?:(?<=[\d\s])℃))"
    _item_reg_str = u"(?:(?:[><]\s*)?" + _number_reg_str + u"(?:\s*)" + _unit_reg_str + ")"
    _weak_item_reg_str = u"(?:(?:[><\*]\s*)?" + _number_reg_str + u"(?:\s*)" + _weak_unit_reg_str + \
                         u"(?!\]))|(?:%{1}" + _number_reg_str + "(?!\]))|(?:" + _number_reg_str + u"[\*×~]{1}" + _number_reg_str + \
                         u"(?:\s*)" + _weak_unit_reg_str + u"(?!\]))"
    _complex_eng_reg_str = u"(?:(?!\d+)(?<![\u4e00-\u9fa5])[A-Za-z0-9]+[\+\-＋－%\._]{1}[^:。：.、,，；;\s@\u4e00-\u9fa5]*)" \
                           u"|(?:(?![0-9]+)(?![A-Za-z]+)[A-Za-z0-9]+)"

    _month_reg_str = u"(?:0?[1-9]|1[0-2])"
    _day_reg_str = u"(?:(?:0[1-9])|(?:(?:1|2)[0-9])|30|31)"
    _date_reg_str1 = u"(?:(?:\d{4}-)?(?<![0-9])" + _month_reg_str + "[\-－]{1}" + _day_reg_str + "(?!-))"
    _date_reg_str2 = u"(?:(?<![\-－])" + _month_reg_str + "." + _day_reg_str + u"[\-－]" + _month_reg_str + "." + _day_reg_str + "(?!-))"
    _date_reg_str3 = u"(?:(?<![\-－])" + _day_reg_str + u"(?:[\-－]" + _day_reg_str + ")" + u"日?(?!-))"
    _date_reg_str4 = u"(?:(?<![\-－])" + _day_reg_str + u"日{1}(?!-))"
    _data_reg_str5 = u"(?:(?<![\-－])\(?[0-9]{2}-)" + _month_reg_str + "-" + _day_reg_str  + u"\)?(?!-)"
    _date_reg_str = u"(?:" + _data_reg_str5 + "|" + _date_reg_str2 + "|" + _date_reg_str3 + "|" + _date_reg_str4 \
                    + "|" + _date_reg_str1 + ")"
    _time_reg_str = u"(?:(?:20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d(?:.\d+)?)"

    _integer_reg_str = u"[1-9][0-9]*|[二三四五六七八九]?十[一二三四五六七八九]|一|二|三|四|五|六|七|八|九|十"

    # 外部接口调用的正则
    PREDEFINED_REGEX = {
        "number":   _number_reg_str,
        "unit":     _weak_unit_reg_str,
        "item":     _weak_item_reg_str,
        "date":     _date_reg_str,
        "time":     _time_reg_str,
        "complex":  _complex_eng_reg_str,
        "num":      _number_reg_str,
        "english":  u"[a-zA-z]+",
        "integer":  _integer_reg_str
    }


global_var = GlobalVar()


