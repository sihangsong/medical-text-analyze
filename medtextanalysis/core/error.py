"""

定义异常类

异常使用方法:

try:
    sth
except someError as e:
    print or do sth
(finally:
    print or do sth)

这会捕获对应异常，并创建实例e，你可以调用

注意不到万不得已不要使用异常，异常在和用户交互的时候才需要，
其主要目的是防止用户向程序传参时的错误以及用户试图访问错误的变量或方法。

"""


class Error(Exception):
    """
    异常基类
    """


class UnknownError(Error):
    """
    未知错误
    """
    def __init__(self,err_msg):
        self.err_msg= err_msg

class TypeError(Error):
    """
    类型错误，用于捕获将错误类型数据传入导致程序无法继续进行的错误

    参数：
        wrong_type: 导致错误的类型
        right_type: 应该使用的类型

    """

    def __init__(self,wrong_type,right_type):
        self.wrong_type= wrong_type
        self.right_type= right_type
        self.err_msg = "Error: Suppose to get %s but get a %s!" % (right_type, wrong_type)


class CixingError(Error):
    """
    词性异常,用于捕获非法词性标注

    参数：
        wrong_label: 导致错误的词性
    """

    def __init__(self, wrong_label):
        self.wrong_label = wrong_label


class InputError(Error):
    """
    输入异常，用于捕获可能导致程序崩溃的异常输入
    (例如，长度为0的输入文档)

    参数:
        wrong_input: 导致错误的输入
    """
    def __init__(self, wrong_input):
        self.wrong_input = wrong_input
