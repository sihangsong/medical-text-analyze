# -*- coding: utf-8 -*-

"""
表示一个最小单元：词语

主要属性就是内容，词性。
基类是 Word

"""
import medtextanalysis.core.error as error
from medtextanalysis.config import global_var

# 这个变量不对外开放接口，所以放在这里
WORD_UNKNOWN_LABEL = "unknown"

class Word(object):
    """
    单词的基类，是最小的语素单位。
    继承类有：标点，名词，形容词，动词等

    基本属性：

    类方法：
    """
    __content= ""
    __len=0
    __label ={
        "cixing": "unknown"
    }


    @property
    def len(self):
        return self.__len

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self,word_content):
        if isinstance(word_content,str):
            self.__content = word_content
        else:
            try:
                self.__content = str(word_content)
            except TypeError as e:
                raise error.UnknownError
        self.__len= len(self.__content)


    # 词特有标注：词性
    @property
    def cixing(self):
        return self.__label["cixing"]

    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self, word_label):
        if not isinstance(word_label,dict):
            try:
                raise error.TypeError(type(word_label),"dict")
            except error.TypeError as e:
                print(e.err_msg)
                exit(0)
        if "cixing" in word_label:
            if word_label["cixing"] not in global_var.CIXING_LIST:
                try:
                    raise error.CixingError(word_label)
                except error.CixingError as e:
                    print("Warning: undetected label %s for word %s." % (e.wrong_label, self.content))
        else:
            word_label["cixing"]=WORD_UNKNOWN_LABEL
        self.__label = word_label

    def __init__(self, word_content, word_label=None):
        """
        初始化一个单词
        :param word_content: 词语内容，应该为字符串
        :param word_label: 对词语的标记,默认为一个只对词性标注不知道的字典
        """
        if word_label is None:
            word_label = {"cixing": WORD_UNKNOWN_LABEL}
        self.content = word_content
        self.label = word_label

    def has_cixing(self):
        return self.cixing != WORD_UNKNOWN_LABEL

    def __str__(self):
        return self.content


class Comma(Word):
    """
    标点

    主要用于完善句子成分，同时处理一些 EB-TAD 之类的混在中文里面的英文术语
    注: 可能没用
    """
    pass


class SingleWord(Word):
    """
    单字，指将一个长度不为1的字符串视为一个字而不是词语，但是它有词性。

    序列化的时候不会将这个类的字符串拆开，视为一个单元(if 我们要做对字的标注)

    什么时候有用：1. 术语(中文/英文/和符号混用)  2. 数字和单位表示一个数值
    """