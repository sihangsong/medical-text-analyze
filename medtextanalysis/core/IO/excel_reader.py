"""
Excel文件的IO

按照要求所述，句子读入的接口是excel文件的 ”内容" 一栏

所以这个重载基类之后就按照这个要求来读入就好了

以后有读入的需求就在这里重载基类/添加方法

"""
from pandas import read_excel as pd_read_excel

import medtextanalysis.core.error as error
from medtextanalysis.core.IO.file_reader import FileReader


class ExcelReader(FileReader):
    filename= ""
    input_content="A"

    def is_file_readable(self):
        """
        判断文件是否是可以读取的
        """
        # 处理异常(读取/文件异常)
        try:
            excel_content = pd_read_excel(self.filename)
        except Exception as e:
            print("ExcelReader:")
            print(e)
            return False
        finally:
            return True

    def is_column_correct(self):
        """
        检查文件是否有指定接口
        :return:
        """
        if not self.is_file_readable():
            return False
        excel_content= pd_read_excel(self.filename)
        if self.input_column not in excel_content:
            try:
                raise error.InputError(self.input_column)
            except error.InputError as e:
                print("ExcelReader:\n "
                      "Cannot find input label with %s" % self.input_column)
                return False
        return True

    def __init__(self,filename,**kwargs):
        """

        :param filename: 相对路径或者绝对路径
        :param input_column: excel中数据读入的接口
        """
        import re
        self.filename= filename
        #if filename[0]=='/':
        #    self.filename= filename[1:]

        # 特判一下空格的问题
        #java_uri_path_regex= "%20"
        #pattern= re.compile(java_uri_path_regex)
        #self.filename=pattern.sub(" ",self.filename)

        self.input_column=kwargs.get("input_column",u"内容")

    def get_content(self):
        """

        :return: pandas读取的excel切片
        """
        if not self.is_column_correct():
            return []
        excel_content = pd_read_excel(self.filename)
        return excel_content[self.input_column]
