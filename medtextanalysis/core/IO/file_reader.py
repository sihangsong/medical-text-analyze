"""
文档阅读的基类

基本上就是把读取文件，返回句子的事情封装一下

各种文件的阅读重载这个类好了

注意除了输入的excel，临时文件的读取也可以重载这个类(如果很复杂的话，当然一般不需要)

"""
from medtextanalysis.util.funcs import force_decode


class FileReader(object):
    filename = ""
    input_content = ""

    def is_file_readable(self):
        try:
            f = open(self.filename,"r")
        except IOError as e:
            print(e)
            return False
        return True

    def __init__(self,name):
        self.filename = name

    def get_content(self):
        if not self.is_file_readable():
            return ""
        with open(self.filename,"rb") as f:
            return force_decode(f.read())