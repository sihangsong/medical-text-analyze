"""
专门输出结果的类


"""
from pandas import DataFrame as pd_dataframe

import medtextanalysis.core.error as error
from medtextanalysis.core.IO.file_writer import FileWriter

class ReportWriter(FileWriter):

    def __init__(self,output_filename):
        """

        :param output_filename:
        """
        self.fileformat="xlsx"
        self.filename=output_filename

        # 特判一下空格的问题
        if self.filename[0]=='/':
            self.filename=self.filename[1:]
        import re
        java_uri_path_regex = "%20"
        pattern = re.compile(java_uri_path_regex)
        self.filename = pattern.sub(" ",self.filename)


    def write_report(self, origin, report):
        """

        :param origin:
        :param report:
        :return:
        """
        save= pd_dataframe({u"原始内容": origin, u"报告": report})
        try:
            save.to_excel(self.filename,index=True,encoding="utf-8",sheet_name="result")
        except IOError as e:
            # 99% 的情况下不会有问题
            # 但有那么1%的情况下写入会失败，原因是给了错误的文件名或者没有写入权限
            print(e.args)
            return False
        return True