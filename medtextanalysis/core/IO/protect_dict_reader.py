"""
阅读保护词典：

保护词典的格式:

每行两个单词，第一个是词语或关键字，第二个是保护内容的词性标记

"""
from medtextanalysis.util.funcs import force_decode


class ProtectDictReader(object):
    filename = ""
    input_content = ""

    def is_file_readable(self):
        try:
            f = open(self.filename,"r")
        except IOError as e:
            print(e)
            return False
        return True

    def __init__(self,name):
        self.filename = name

    def get_content(self):
        if not self.is_file_readable():
            return ""
        with open(self.filename,"rb") as f:
            return force_decode(f.read())

    def parse_protect_dict(self):
        file_content = self.get_content()
        dicts= {}
        import re
        pattern = re.compile("\s+")

        for each_line in file_content.splitlines():
            each_line = pattern.sub(" ",each_line)
            each_line = each_line.lstrip(" ").rstrip(" ")
            if len(each_line) == 0 or each_line == " ":
                continue
            parts= each_line.split(" ")
            if len(parts) != 2:
                continue
            word = parts[0]
            label = parts[1]
            dicts[word] = label

        return dicts
