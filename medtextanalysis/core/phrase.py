
"""
短语类，是词语的复合，比如 n.+n. (今天/上午)，或者 d.+d. (无/明显)或者 n.+ d.+adj. (心率/不/齐)

由于输入文本的特殊性，有的时候一个短语可能就单独成为一个句子，有的时候它们就作为句子的成分。
注意句子也不一定完全由短语组成，也可能是若干单词组成，短语不是语素

我们希望算法可以判定短语的成分或者是修饰关系, 或者是在实际句子中的功能

此外短语类有一类特殊的情形，格式是: nz.+ [:] + [箭头/+/-/>/<] + 数值 + [单位]
这个短语混在中文里给处理造成很多困难，而且一般会指向二级报告，所以一旦发现要立即处理

基类是 Phrase

子类有 报告条目(固定为专有名词 + [修饰符号] + 数值 + [单位])，复合专有名词，偏正短语

"""
from medtextanalysis.core import error
from medtextanalysis.core.word import Word


class Phrase(object):
    """
    短语的基类，短语由单词组成

    属性：
        len: 短语由几个词语组成
        words: 词语列表，为一个Word类的list
        content: 连在一起的短语字符串：如：今天上午， HCP>1.6*10^9/mol
        content_len: 这个短语占多少个字符

    类方法：
        append
        prepend

    """

    __words = []
    __label = {}

    def __len__(self):
        return len(self.__words)

    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self,phrase_label):
        if not isinstance(phrase_label,dict):
            try:
                raise error.TypeError(type(phrase_label),"dict")
            except error.TypeError as e:
                print(e.err_msg)
                exit(0)
        self.__label = phrase_label

    @property
    def words(self):
        return self.__words

    @words.setter
    def words(self,word_list):
        self.__words = []
        for word in word_list:
            self.append(word)

    @property
    def content(self):
        return "".join([w.content for w in self.__words])

    @property
    def len(self):
        return len(self.__words)

    @property
    def content_len(self):
        return len(self.content)

    def __init__(self,words=list([]),phrase_label={}):
        if not isinstance(words,list):
            try:
                raise error.TypeError(type(words),"list")
            except error.TypeError as e:
                print(e.err_msg)
                return False
        self.words= words

    # 这里是为了使得类可以支持循环操作(创建Iterator)
    # 这个没什么特别的好处，只是好玩
    def __iter__(self):
        self.__count = -1
        return self

    def __next__(self):
        self.__count += 1
        if self.__count == self.len:
            raise StopIteration()
        return self.__words[self.__count]

    # 这个可以让类支持下标访问,如 A=Phrase(sth), A[1]
    # 这个也可以写成字典的形式
    def __getitem__(self, item):
        return self.__words[item]

    def append(self,word):
        """
        在末尾添加成分
        :param word:
        :return: nothing
        """
        if not isinstance(word,Word):
            if not isinstance(word,str):
                try:
                    raise error.TypeError(type(word),"Word")
                except error.TypeError as e:
                    print(e.err_msg)
                    return False
            else:
                self.__words.append(Word(word))
        else:
            self.__words.append(word)
        return True

    def prepend(self,word):
        """
        在开头添加成分
        请小心使用
        :param word:
        :return:
        """
        if not isinstance(word,Word):
            if not isinstance(word,str):
                try:
                    raise error.TypeError(type(word),"Word")
                except error.TypeError as e:
                    print(e.err_msg)
                    return False
            else:
                word = Word(word)
        self.__words = [word] + self.__words
        return True

