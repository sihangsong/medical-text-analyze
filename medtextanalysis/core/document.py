
"""
文档/段落类，本质是句子和短语的组合(不完全是句子的组合，因为数据的原因)

该类别接收的输入终于是原始字符串了，它将对原始字符串按照标记/按照算法进行分解，
用一个句子的列表进行存储，同时这个类应该提供序列化数据和句子之间的耦合。

比较麻烦的几个问题,视情况将决定使用类实现还是作为内置方法实现：
    标记数据的清理和验证 —— 专门的脚本位于 data_processing 模块下，这个类应该调用它们
    序列化标注  —— 由专门的标记序列处理。文档类不直接涉及算法，只是把输入以句子为单位分发下去
    文档生成 —— 这个应该是这个类调用求解器或者函数实现的

"""

import medtextanalysis.core.error as error
import medtextanalysis.data_processing.data_cleaning as data_cleaning
import medtextanalysis.data_processing.data_labeling as data_labeling
from medtextanalysis.config import GlobalVar

class Document(object):
    """
    文档的基类

        这个类接受字符串作为输入，输出包括但不仅限于：
            序列化标记的数据(相当于训练结果)
            文档的断句结果
            文档的提取信息的报告

    注意为了区分训练集和测试样本，训练集的文档类是下面的继承类。因为字符串处理方式不同


    """
    __content = ""
    sentence_list = []
    label_from_data_list = []


    # 标记文档是否被训练过
    __is_trained= False

    # 标记文档数据是否经过内置方法的清洗
    # 你要是清洗过了，请在初始化中设置对应选项为true防止调用内部算法
    # 这里会给一种我们实现的方法， 但是也重载可以写自己的
    __is_data_cleaned= False

    # 我们不允许对这个类的初始字符串进行重新赋值
    # 所以content只是只读属性
    @property
    def content(self):
        return self.__content

    @property
    def length(self):
        return len(self.__content)

    @property
    def sentences(self):
        return self.sentence_list

    def __init__(self, input_str, **kwargs):
        """
        读入文档，假定input_str是未清洗过的数据
        :param input_str:
        :param kwargs:
        """
        if not isinstance(input_str, str):
            raise error.TypeError(type(input_str),"str")

        self.__content = input_str
        __is_data_cleaned = kwargs.get("is_cleaned", False)
        self.keep_separator = kwargs.get("keep_separator", False)

        # 如果没有清洗过，需要先处理
        if __is_data_cleaned is not True:
            self._clean_content()
            self.__is_data_cleaned= True

    def __len__(self):
        return len(self.sentences)


    def _clean_content(self):
        """
        内置清洗数据的函数
        这里和标记数据的处理是不同的
        :return:
        """
        pass


    def _generate_sentences(self):
        """
        给文档断句，生成句子类
        :return:
        """

class LabeledDocument(Document):
    """
    标记了的训练集文档

    """
    __is_data_cleaned = False
    __is_trained = False

    def _clean_content(self):
        """
        重载的清洗函数，因为要提取出一个标签
        这里和未标记的训练集的方法略有不同
        :return:
        """
        if self.__is_data_cleaned:
            return

        sentence_list, reg_dict = _document_cleaning(self.content,remain_separator=self.keep_separator)

        # 得到标注标签和正则内容的标签
        self.sentence_list = []
        self.label_from_data_list = []
        for each in sentence_list:
            phrases, labels = data_labeling.extract_train_data_and_labels(each)
            self.sentence_list.append(phrases)
            self.label_from_data_list.append(labels)

        for i in range(len(sentence_list)):
            # 对正则进行标注
            self.label_from_data_list[i] = data_labeling.get_protected_regex_label(
                self.sentence_list[i],
                self.label_from_data_list[i],
            )

            #还原正则
            self.sentence_list[i] = data_cleaning.recover_from_regex(self.sentence_list[i],reg_dict)

        self.__is_data_cleaned = True


    def _generate_sentences(self):
        """
        重载的生成句子类，因为要把取出的标签塞到每个成分里
        然后标注类才能识别它们生成特征模板
        :return:
        """
        pass

    def is_unknown_label_exists(self):
        from medtextanalysis.config import GlobalVar as _var
        flag = True
        for i in range(len(self.sentence_list)):
            phrases = self.sentence_list[i]
            labels = self.label_from_data_list[i]
            for j in range(len(phrases)):
                if labels[j] == "unknown" and phrases[j] not in _var.ALL_SPECIAL_SYMBOL_LIST:
                    print(phrases[j],labels[j])
                    flag= False
        return flag

def _document_cleaning(document, remain_separator=False):
    """

    :param document:
    :return:
    """
    #print(document)
    # 特殊字符统一
    document = data_cleaning.unify_special_symbols(document)

    # 扔掉无用字符
    # document = data_cleaning.remove_separator(document, separator=["\)"], replace_chr="")
    document = data_cleaning.remove_separator(document, separator=["【",u"】"],replace_chr=" ")
    document = data_cleaning.remove_separator(document, separator=["\s"], replace_chr=" ")
    document = data_cleaning.remove_separator(document,separator=GlobalVar.USELESS_WORD_LIST,replace_chr="")
    document = data_cleaning.remove_separator(document, separator=["\s"], replace_chr=" ")
    # document = data_cleaning.remove_white_space(document)
    # 正则保护
    # 目的有二： 1,防止数据传入分词，序列标注等方法时对于某些字段的处理错误; 2,对特殊词语实行特殊标注
    reg_item_dict = {}
    regex_dict = GlobalVar.PREDEFINED_REGEX

    # 正则保护会把正则匹配到的内容替换为{name}[num] 的形式，这样方便换回来
    # 注意这件事情必须要在断句之前做，所以正则保护是document-level的事情，千万不能写成对每句话做，不然特殊符号就被断开了。
    # (举例，区分时间中的":"和报告开头的":", 当然你可以特判，但是这样不能用正则了)
    document, time_items = data_cleaning.replace_by_regex(document, regex_dict["time"], "{time}",
                                                          remain_match_groups=True, indexing=True)
    reg_item_dict["time"] = time_items
    document, date_items = data_cleaning.replace_by_regex(document, regex_dict["date"], "{date}",
                                                          remain_match_groups=True, indexing=True)
    reg_item_dict["date"] = date_items
    document, complex_eng_items = data_cleaning.replace_by_regex(document, regex_dict["complex"], "{complex_eng}",
                                                                 remain_match_groups=True, indexing=True)
    reg_item_dict["complex_eng"] = complex_eng_items
    document, item_items = data_cleaning.replace_by_regex(document, regex_dict["item"], "{item}",
                                                          remain_match_groups=True, indexing=True)
    reg_item_dict["item"] = item_items
    document, num_items = data_cleaning.replace_by_regex(document, regex_dict["num"], "{num}",
                                                         remain_match_groups=True, indexing=True)
    reg_item_dict["num"] = num_items
    # 断句
    sentences, sentence_separator = data_cleaning.split_by_symbol(document, remain_separator=False)
    word_list = list([])

    import re
    symbol_pattern = re.compile("{(?:[A-Za-z_]+)}\[(?:\d+)\]")

    # 对每句话断成份
    for each,each_separator in zip(sentences,sentence_separator):
        # 分成 成分
        if not remain_separator:
            input = each
        else:
            input = each+each_separator
        phrases, separator = data_cleaning.split_by_symbol(each+" "+each_separator,
                                                           separator=GlobalVar.SEPARATE_WORD_LIST,
                                                           remain_separator=remain_separator)

        if len(phrases) > 0:
            if not remain_separator:
                word_list.append(phrases)
            else:
                # remove white space
                good_phrases = list([])
                for w in phrases:
                    w = w.lstrip(" ").rstrip(" ")
                    if len(w) == 0:
                        continue

                    p , s = data_cleaning.split_by_regex(w,u"{[A-Za-z_]+}\[\d+\]",remain_match_groups=True)

                    good_phrases.extend(p)
                word_list.append(good_phrases)

    # 返回非常牛逼的分割列表和正则列表
    return word_list, reg_item_dict


