"""

序列化标注：无论是分词，实体识别，还是关键词抽取，序列标注都是一种最常用的标记数据的手段

这里定义序列化标注基类  SeqLabel

该类接受文档类作为输入参数，根据已知信息返回序列化标注数据，同时也可以根据标注结果对输入句子/文档进行解构。

可以通过重载该基类得到不同的结果

"""
import medtextanalysis.core.error as error
import medtextanalysis.data_processing.data_labeling as datalabel
from medtextanalysis.core.document import Document

# TODO (wy): 把打标签和模板生成这两件事情分开，因为test数据没有标签。

class SeqLabel(object):
    """

    参数:
        labeled_content : 一个列表，列表中每个元素是字典，主要包括内容和标注，但是不建议从外部访问它
        labels: 属性，用于访问labeled_content
        len(SeqLabel): 可以直接询问长度


    方法：
        __init__ : 初始化
        _do_labeling : 主要接口，用于把Document转化为你需要的各种可能的label
    """

    model_contents = list([])
    label_contents = list([])
    word_contents =list([])

    # 注意python的类继承的时候前面带下划线的变量继承方式及其诡异，重载时要重新定义一次这些私有变量
    # 私有方法也是
    __is_labeled= False

    def __init__(self,input_document,**kwargs):
        """

        :param input_document:
        :param kwargs:
        """

        # 检查输入合法性
        if not isinstance(input_document, Document):
            try:
                raise error.TypeError(type(input_document),"Document")
            except error.TypeError as e:
                print(e.err_msg)

        self.__reserve_unknown = kwargs.get("reserve_unknown", False)

        self.model_contents= list([])
        self.label_contents= list([])
        self.word_contents = list([])
        self._do_labeling(input_document)


    def _do_labeling(self,document):
        """
        打标签程序，根据存储的文档来生成标签
        请主要实现这个部分
        以下是一个例子
        :param document:
        :return:
        """
        sentence_num = len(document)

        # 第一步: 将中文单词/英文单词/未知项目 拆开成字的单元并追加单字位置标注(不拆开的词语标为单字)
        def __label_phrase(content, data_label, addition_label):
            """
            将单词打断成单个字，并加以标注
            :param content:
            :param data_label:
            :param addition_label:
            :return:
            """
            template_list = list([])
            label_list=list([])

            if len(content) == 1:
                temp_dict =dict({
                    "word": content,
                    "type": datalabel.get_chr_type(content),
                    },**addition_label)
                if data_label == "unknown" and temp_dict["type"] != "unknown":
                    return [temp_dict],["s-"+temp_dict["type"]]
                else:
                    return [temp_dict],["s-"+data_label]

            for index in range(len(content)):
                temp_dict = dict({
                    "word": content[index],
                    },**addition_label)
                if index == 0:
                    # 词首
                    postag = "b-" + data_label
                    temp_dict["is_sentence_end"] = False
                    temp_dict["is_doc_end"] = False
                elif index == len(content)-1:
                    # 词尾
                    postag = "e-" + data_label
                    temp_dict["is_sentence_begin"] = False
                    temp_dict["is_doc_begin"] = False
                else:
                    # 词中
                    postag = "m-" + data_label
                    temp_dict["is_sentence_end"] = False
                    temp_dict["is_doc_end"] = False
                    temp_dict["is_sentence_begin"] = False
                    temp_dict["is_doc_begin"] = False

                temp_dict["type"] = datalabel.get_chr_type(content[index])
                if data_label == "unknown" and (temp_dict["type"] not in ["unknown","cn"]):
                   postag = postag[0:2]+temp_dict["type"]
                template_list.append(temp_dict)
                label_list.append(postag)

            return template_list,label_list

        def __label_single_word(content, data_label, addition_label):
            # 单字，顾名思义，就是无论多长的东西都看成一个语素
            # 注意在中文和英语/符号/数字混用的情况下，不建议将此类东西看作单字类
            # 因为这样区分度太低了
            # 比较适合单字类的东西：日期/时间/数值+单位/一串特殊符号(例如++++)
            return dict({
                "word": content,
                "type": data_label,
                },**addition_label), "s-"+data_label

        unigram_model_list = list([])

        for i in range(sentence_num):
            phrases = document.sentences[i]
            labels = document.label_from_data_list[i]
            for j in range(len(phrases)):
                phrase = phrases[j]
                label = labels[j]

                # 生成首字符，尾字符，中文等特征标识
                addition_dict = dict()
                addition_dict["is_doc_begin"] = (i == 0 and j == 0)
                addition_dict["is_doc_end"] = (i == sentence_num - 1 and j == len(phrases) - 1)
                addition_dict["is_sentence_begin"] = (j == 0)
                addition_dict["is_sentence_end"] = (j == len(phrases) - 1)

                if label in ["complex", "eng", "item", "date", "time"]:
                    # 这些看作单字处理
                    result_dict, gt = __label_single_word(phrase,label,addition_dict)
                    unigram_model_list.append(result_dict)
                    self.label_contents.append(gt)
                    self.word_contents.append(result_dict["word"])

                elif label != "unknown" or (label == "unknown" and self.__reserve_unknown):
                    # 全部打散(包括不知道是什么的)
                    # TODO: (wy) 纯英文专业术语也是标记为nz的，但是它们会不会当成单字会好点?
                    result_dicts, gt = __label_phrase(phrase, label, addition_dict)
                    unigram_model_list.extend(result_dicts)
                    self.label_contents.extend(gt)
                    for each in result_dicts:
                        self.word_contents.append(each["word"])

                else:
                    pass
        # 第二步：根据词性和位置标注生成 crf所需要的特征模板
        # 在这里主要完成上下文的构建
        # 这里只考虑上一个和下一个(3-gram)，可以根据需要创建更复杂的特征模板
        # TODO (wy): 重载或重写这一段，完成更复杂的特征模板的设计
        self.model_contents = list()

        element_num = len(unigram_model_list)
        for i in range(element_num):
            unigram_model = unigram_model_list[i]

            features= [
                "word=%s" % unigram_model["word"],
                "word.type=%s" % unigram_model["type"]
            ]
            if unigram_model["is_doc_begin"]:
                features.append("BOD")
            if unigram_model["is_doc_end"]:
                features.append("EOD")
            if unigram_model["is_sentence_begin"]:
                features.append("BOS")
            if unigram_model["is_sentence_end"]:
                features.append("EOS")
#==============================================================================
#             if (i>1 and i < element_num - 2):     #-2~+2模板表
#                 unigram_model1 = unigram_model_list[i-2]
#                 unigram_model2 = unigram_model_list[i-1]
#                 unigram_model3 = unigram_model_list[i]
#                 unigram_model4 = unigram_model_list[i+1]
#                 unigram_model5 = unigram_model_list[i+2]
#                 features.extend([
#                     "-2:word=%s" % unigram_model1["word"],"-1:word=%s" % unigram_model2["word"],"0:word=%s" % unigram_model3["word"],"+1:word=%s" % unigram_model4["word"],"+2:word=%s" % unigram_model5["word"],
#                     "-2:word.type=%s" % unigram_model1["type"],"-1:word.type=%s" % unigram_model2["type"],"0:word.type=%s" % unigram_model3["type"],"+1:word.type=%s" % unigram_model4["type"],"+2:word.type=%s" % unigram_model5["type"],
#                     "-1:word=%s / 0:word=%s" % (unigram_model2["word"],unigram_model3["word"]),"0:word=%s / +1:word=%s" % (unigram_model3["word"],unigram_model4["word"]),
#                     "-1:word.type=%s / 0:word.type=%s" % (unigram_model2["type"],unigram_model3["type"]),"0:word.type=%s / +1:word.type=%s" % (unigram_model3["type"],unigram_model4["type"]),
#                     "-2:word=%s / -1:word=%s / 0:word=%s" % (unigram_model1["word"],unigram_model2["word"],unigram_model3["word"]),
#                     "-1:word=%s / 0:word=%s / +1:word=%s" % (unigram_model2["word"],unigram_model3["word"],unigram_model4["word"]),
#                     "0:word=%s / +1:word=%s / +2:word=%s" % (unigram_model3["word"],unigram_model4["word"],unigram_model5["word"]),
#                     "-2:word.type=%s / -1:word.type=%s / 0:word.type=%s" % (unigram_model["type"],unigram_model2["type"],unigram_model3["type"]),
#                     "-1:word.type=%s / 0:word.type=%s / +1:word.type=%s" % (unigram_model2["type"],unigram_model3["type"],unigram_model4["type"]),
#                     "0:word.type=%s / +1:word.type=%s / +2:word.type=%s" % (unigram_model3["type"],unigram_model4["type"],unigram_model5["type"]),
#                 ])
#==============================================================================
            if (i > -1):
                unigram_model3 = unigram_model_list[i]
                features.extend([
                    "0:word=%s" % unigram_model3["word"],
                    "0:word.type=%s" % unigram_model3["type"],
                ])
            if (i > 0):
                unigram_model2 = unigram_model_list[i-1]
                unigram_model3 = unigram_model_list[i]
                features.extend([
                    "-1:word=%s" % unigram_model2["word"],
                    "-1:word.type=%s" % unigram_model2["type"],
                    "-1:word=%s / 0:word=%s" % (unigram_model2["word"],unigram_model3["word"]),
                    "-1:word.type=%s / 0:word.type=%s" % (unigram_model2["type"],unigram_model3["type"]),
                ])
            # if (i > 1):
            #     unigram_model1 = unigram_model_list[i-2]
            #     unigram_model2 = unigram_model_list[i-1]
            #     unigram_model3 = unigram_model_list[i]
            #     features.extend([
            #         "-2:word=%s" % unigram_model1["word"],
            #         "-2:word.type=%s" % unigram_model1["type"],
            #         "-2:word=%s / -1:word=%s / 0:word=%s" % (unigram_model1["word"],unigram_model2["word"],unigram_model3["word"]),
            #         "-2:word.type=%s / -1:word.type=%s / 0:word.type=%s" % (unigram_model["type"],unigram_model2["type"],unigram_model3["type"]),
            #     ])
            if (i < element_num - 1):
                unigram_model3 = unigram_model_list[i]
                unigram_model4 = unigram_model_list[i+1]
                features.extend([
                    "+1:word=%s" % unigram_model4["word"],
                    "+1:word.type=%s" % unigram_model4["type"],
                    "0:word=%s / +1:word=%s" % (unigram_model3["word"],unigram_model4["word"]),
                    "0:word.type=%s / +1:word.type=%s" % (unigram_model3["type"],unigram_model4["type"]),
                ])
            # if (i < element_num - 2):
            #     unigram_model3 = unigram_model_list[i]
            #     unigram_model4 = unigram_model_list[i+1]
            #     unigram_model5 = unigram_model_list[i+2]
            #     features.extend([
            #         "+2:word=%s" % unigram_model5["word"],
            #         "+2:word.type=%s" % unigram_model5["type"],
            #         "0:word=%s / +1:word=%s / +2:word=%s" % (unigram_model3["word"],unigram_model4["word"],unigram_model5["word"]),
            #         "0:word.type=%s / +1:word.type=%s / +2:word.type=%s" % (unigram_model3["type"],unigram_model4["type"],unigram_model5["type"]),
            #     ])
            if(i > 0 and i < element_num - 1):
                unigram_model2 = unigram_model_list[i-1]    
                unigram_model3 = unigram_model_list[i]
                unigram_model4 = unigram_model_list[i+1]
                features.extend([
                    "-1:word=%s / 0:word=%s / +1:word=%s" % (unigram_model2["word"],unigram_model3["word"],unigram_model4["word"]),
                    "-1:word.type=%s / 0:word.type=%s / +1:word.type=%s" % (unigram_model2["type"],unigram_model3["type"],unigram_model4["type"]),
                ])                

            self.model_contents.append(features)


    # 这里是为了使得类可以支持循环操作(创建Iterator)
    # 这个没什么特别的好处，只是好玩

    @property
    def model(self):
        return self.model_contents

    @property
    def label(self):
        return self.label_contents

    @property
    def input(self):
        return "".join(self.word_contents)

    def __len__(self):
        return len(self.model_contents)

    def __iter__(self):
        self.__count = -1
        return self

    def __next__(self):
        self.__count += 1
        if self.__count == len(self):
            raise StopIteration()
        return self.model_contents[self.__count]

    # 这个可以让类支持下标访问
    # 这个也可以写成字典的形式

    def __getitem__(self, item):
        return self.model_contents[item]
