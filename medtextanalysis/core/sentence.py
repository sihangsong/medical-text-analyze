# -*- coding: utf-8 -*-

"""
整个工程的一个基类，很多算法都是以句子为单元展开的。

而句子又是由各式各样的词语和短语组成的

对于这个类，我们希望它的功能是：

    接受由词语和短语组成的列表作为输入，
    它可以存储词语/短语序列。

"""

from medtextanalysis.core import error
from medtextanalysis.core.phrase import Phrase
from medtextanalysis.core.word import Word


class Sentence(object):

    """
    Sentence 句子的基类

    属性:
        elements: 句子的组成成分
        len: 句子组成成分的个数(列表elements的长度)
        content: 句子的字符串表示
        content_len: 句子长度
    方法:

    """

    __elements= []
    __label= {}

    @property
    def content(self):
        return "".join([w.content for w in self.__elements])

    @property
    def elements(self):
        return self.__elements

    @property
    def len(self):
        return len(self.__elements)

    @staticmethod
    def _is_valid_item(self, data):
        return isinstance(data, Word) or isinstance(data, Phrase) or isinstance(data, str)

    def is_item_word(self,item_index):
        return isinstance(self.__elements[item_index],Word)

    def is_item_phrase(self, item_index):
        return isinstance(self.__elements[item_index],Phrase)

    def __init__(self,src_data=list([]),**kwargs):
        """
        句子类的初始化，输入是字和词组的list
        (根据字符串构建的部分在序列化标注和文档类里面实现，这个类只存储分解后的结果)
        :param src_data:
        :param kwargs:
        """
        if not isinstance(src_data,list):
            try:
                raise error.TypeError(type(src_data),"list")
            except error.TypeError as e:
                print(e.err_msg)
                return
        for data in src_data:
            self.append(data)

    def append(self, data):
        """
        在末尾添加成份
        :param: data: 输入实体(字符串/字/词)
        :return: nothing
        """
        if not self._is_valid_item(data):
            try:
                raise error.TypeError(type(data),"Word/Phrase/str")
            except error.TypeError as e:
                print(e.err_msg)
                return False
        if isinstance(data,str):
            data = Word(data)
        self.__elements.append(data)
        return True

    def prepend(self, data):
        """
        在开头添加成份
        请小心使用
        :param: data: 输入实体(字符串/字/词)
        :return: nothing
        """
        if not self._is_valid_item(data):
            try:
                raise error.TypeError(type(data),"Word/Phrase/str")
            except error.TypeError as e:
                print(e.err_msg)
                return False
        if isinstance(data,str):
            data = Word(data)
        self.__elements=[data]+ self.__elements
        return True

    def __len__(self):
        return len(self.__elements)

    def __iter__(self):
        self.__count = -1
        return self

    def __next__(self):
        self.__count += 1
        if self.__count == self.len:
            raise StopIteration()
        return self.elements[self.__count]

    def __getitem__(self, item):
        return self.elements[item]

    def __str__(self):
        pass

    def __repr__(self):
        pass

