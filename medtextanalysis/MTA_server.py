# -*- coding:utf-8 -*-

"""
thrift实现的服务器接口，
thrift模板生成的文件会放在 ./protocol 目录下
作为服务器端，需要为每个调用的API提供实例化接口

实例化的接口如下:

checkname(name,id)                  检查服务器名称，如果没有问题为该客户端id初始化服务
ping(id)                            检查服务器是否可以正常提供服务
clean(id)                           销毁编号为id的客户端留下的临时记录
log(id)                             返回服务器端关于某个客户端的日志文件
pass_bytes(bytes,file_type,id)      接受传递的文件
predict(str,int radius)             预测接口

"""


# 调用Server的内容
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

import time
from mmap import mmap

import os

# 判断平台
import platform
import sys
sys.path.append("..")

from medtextanalysis.config import Config
from medtextanalysis.protocol import MedTextAnalyzer as MTA

from medtextanalysis.algorithm.tagger import CRFTagger
from medtextanalysis.algorithm.report_generator import Report
from medtextanalysis.core.sequalized_label import SeqLabel
from medtextanalysis.core.document import Document,LabeledDocument
from medtextanalysis.data_processing.protect_labeling import add_label_by_protect_dict
from medtextanalysis.util.funcs import force_decode
import medtextanalysis.util.file_format_check as FormatCheck


ROOTDIR = os.getcwd()
#ROOTDIR = os.path.dirname(__file__)

# 实例化server
class MedTextAnalyzerHandler(object):

    index = 0

    __client_not_found_respond = MTA.RespondStatusCode(404, "Client not found!")
    __internal_error_respond =  MTA.RespondStatusCode(-1, "General Internal Error!")
    __file_format_error_respond = MTA.RespondStatusCode(-2, "General FileStream Format Error!")
    __io_error_respond = MTA.RespondStatusCode(400, "General IO Error")
    __ok_respond = MTA.RespondStatusCode(1,"OK!")

    # 这是最简单的统计我们实例化了多少个类的方法
    @classmethod
    def count_index(cls):
        cls.index += 1



    def __init__(self,server_name="Default"):
        """
        启动的server
        :param server_name:
        """
        self.log={}

        # 生成默认配置文件
        self.conf = Config()
        self.__is_CRF_trained= False

        self.name = server_name
        self.tagger = None
        self.report_generator = None
        self.count_index()
        self.__client_id_config={}

    def checkname(self,name,_id):
        """
        程序检查服务器名称是否正确,
        如果正确,将为第一次连接的_id分配设定
        :param name:
        :param _id:
        :return:
        """
        time.sleep(0.001)
        print("%s with name %s is connecting!" % (_id,name))
        if not isinstance(name,str) or not isinstance(_id,str):
            return False
        if self.name != name:
            return False

        try:
            if _id not in self.__client_id_config:
                # 为第一次连接的_id新建一个设置
                self.__client_id_config[_id] = Config(_id,ROOTDIR)
        except Exception as e:
            return False

        return True

    # TODO(wy): 这个太丑了，一定要移除
    def __parse_protect_dict(self, content):
        """
        属于封装失误了，这个逻辑不知道放哪里比较好了，暂时放在这里

        :param content:
        :return:
        """
        dicts = {}
        import re
        pattern = re.compile("\s+")

        for each_line in content.splitlines():
            each_line = pattern.sub(" ", each_line)
            each_line = each_line.lstrip(" ").rstrip(" ")
            if len(each_line) == 0 or each_line == " ":
                continue
            parts = each_line.split(" ")
            if len(parts) != 2:
                continue
            word = parts[0]
            label = parts[1]
            dicts[word] = label

        return dicts

    def _get_report_json(self, document, radius, _id):
        """
        拿到最关键的json序列字符串，就是算法的核心了
        :param document:
        :param radius:
        :param _id:
        :return:
        """
        Tagger=  CRFTagger()
        Tagger.open(self.__client_id_config[_id].temp_crf_model_filename)

        protect_dict_str = self.__client_id_config[_id].protect_dict_stream.read()
        protect_dict_str = force_decode(protect_dict_str)
        document = add_label_by_protect_dict(document, self.__parse_protect_dict(protect_dict_str))

        A = LabeledDocument(document, keep_separator=True)

        B = SeqLabel(A, reserve_unknown=True)

        label, word = Tagger.tag(B, result_format="dual")
        C = Report()
        C.set_input(word, label)
        C.set_radius(radius)

        search_dict_str = self.__client_id_config[_id].search_dict_stream.read()
        search_dict_str = force_decode(search_dict_str)
        self.__client_id_config[_id].search_dict_stream.seek(0)
        C.set_search_dict(search_dict_str)

        merge_dict_str = self.__client_id_config[_id].merge_dict_stream.read()
        merge_dict_str = force_decode(merge_dict_str)
        self.__client_id_config[_id].merge_dict_stream.seek(0)
        C.set_merge_dict(merge_dict_str)

        #print("Here!")
        return C.generate_json_report()

    def _run_server_test(self,_id):
        test_str = u"今天天气真好"
        print("Running test for client %s" % _id)
        try:
            self._get_report_json(test_str,5,_id)
        except Exception as e:
            return False
        return True

    def ping(self,_id):
        """
        完整检查并运行脚本
        :param _id:
        :return:
        """
        if _id not in self.__client_id_config:
            return self.__client_not_found_respond
        conf = self.__client_id_config[_id]
        assert isinstance(conf, Config)

        #1. 检查文件格式是否正确

        if conf.merge_dict_stream is None or conf.search_dict_stream is None or conf.protect_dict_stream is None:
            return self.__file_format_error_respond
        if not FormatCheck.check_crfmodel_validity(conf.temp_crf_model_filename):
            return self.__file_format_error_respond

        #2. 跑一个测试
        if self._run_server_test(_id):
            return self.__ok_respond
        else:
            return MTA.RespondStatusCode(-200, "Server cannot pass simple predict test")

    def clean(self,_id):
        """
        清理所有资源
        :param _id:
        :return:
        """
        if _id not in self.__client_id_config:
            return self.__client_not_found_respond

        conf = self.__client_id_config[_id]

        #assert(conf,Config)
        if os.path.isfile(conf.temp_crf_model_filename):
            os.remove(conf.temp_crf_model_filename)

        if os.path.isfile(conf.temp_log_filename):
            os.remove(conf.temp_log_filename)

        if conf.search_dict_stream is not None:
            conf.search_dict_stream.flush()
            conf.search_dict_stream.close()
            del conf.search_dict_stream

        if conf.merge_dict_stream is not None:
            conf.merge_dict_stream.flush()
            conf.merge_dict_stream.close()
            del conf.merge_dict_stream

        if conf.protect_dict_stream is not None:
            conf.protect_dict_stream.flush()
            conf.protect_dict_stream.close()
            del conf.protect_dict_stream

        del self.__client_id_config[_id]

        return self.__ok_respond

    def log(self,_id):
        if _id not in self.__client_id_config:
            return bytes()
        try:
            with open(self.__client_id_config[_id].temp_log_filename,"rb") as f:
                return f.readall()
        except IOError as e:
            return bytes()
        except Exception as e:
            return bytes()

    def Test(self,file_content):
        pass

    def pass_bytes(self,file_content,file_type,_id):
        """

        :param file_content:
        :param file_type:
        :param _id:
        :return:
        """
        #assert isinstance(file_type,MTA.ByteStreamType)
        assert isinstance(file_content,bytes)

        # 找不到id就不做了
        if _id not in self.__client_id_config:
            return self.__client_not_found_respond

        assert isinstance(self.__client_id_config[_id],Config)

        # 分别处理输入文件
        if file_type == MTA.ByteStreamType.CRFMODEL:
            # CRF模型,直接写入到指定好的文件位置
            try:
                tmp_filename= self.__client_id_config[_id].temp_crf_model_filename+".tmp"
                #print(tmp_filename)
                #print(self.__client_id_config[_id].temp_crf_model_filename)

                with open(tmp_filename,"wb") as f:
                    f.write(file_content)
                tag = FormatCheck.check_crfmodel_validity(tmp_filename)
                if tag == "IO Error" or tag == "FileNotFound Error":
                    return self.__internal_error_respond
                elif tag == "Format Error":
                    return self.__file_format_error_respond
                else:
                    pass
                if os.path.isfile(tmp_filename):
                    os.remove(tmp_filename)
                else:
                    return self.__io_error_respond
                # 真正确认没问题才记录
                with open(self.__client_id_config[_id].temp_crf_model_filename,"wb") as f:
                    #print("Should write model into %s " % self.__client_id_config[_id].temp_crf_model_filename)
                    f.write(file_content)
            except IOError as e:
                return self.__io_error_respond
            except Exception as e:
                return self.__internal_error_respond

        elif file_type == MTA.ByteStreamType.SEARCHDICT:
            if len(file_content) == 0:
                file_content = "*#06#".encode("UTF-8")

            if not FormatCheck.check_search_dict_validity(file_content):
                return self.__file_format_error_respond
            # 搜索词典,作mmap映射
            if platform.system()=="Windows":
                #Windows 系统一类定义
                searchdict_map = mmap(-1,len(file_content))
            else:
                #Unix 系统一类定义
                searchdict_map = mmap(-1,len(file_content))

            searchdict_map.write(file_content)
            # 复位,不然指针指向了最后一位就读不出来了
            searchdict_map.seek(0)

            # 先删除记录着的
            if self.__client_id_config[_id].search_dict_stream is not None:
                assert isinstance(self.__client_id_config[_id].search_dict_stream, mmap)
                self.__client_id_config[_id].search_dict_stream.flush()
                self.__client_id_config[_id].search_dict_stream.close()
                del self.__client_id_config[_id].search_dict_stream
                self.__client_id_config[_id].search_dict_stream = None

            self.__client_id_config[_id].search_dict_stream = searchdict_map

        elif file_type == MTA.ByteStreamType.MERGEDICT:
            if len(file_content) == 0:
                file_content = "*#06#".encode("UTF-8")

            if not FormatCheck.check_merge_dict_validity(file_content):
                return self.__file_format_error_respond

            # 搜索词典,作mmap映射
            if platform.system()=="Windows":
                #Windows 系统一类定义
                mergedict_map = mmap(-1,len(file_content))
            else:
                #Unix 系统一类定义
                mergedict_map = mmap(-1,len(file_content))

            mergedict_map.write(file_content)
            # 复位,不然指针指向了最后一位就读不出来了
            mergedict_map.seek(0)

            # 先删除记录着的
            if self.__client_id_config[_id].merge_dict_stream is not None:
                assert isinstance(self.__client_id_config[_id].merge_dict_stream, mmap)
                self.__client_id_config[_id].merge_dict_stream.flush()
                self.__client_id_config[_id].merge_dict_stream.close()
                del self.__client_id_config[_id].merge_dict_stream
                self.__client_id_config[_id].merge_dict_stream = None

            self.__client_id_config[_id].merge_dict_stream = mergedict_map
        elif file_type == MTA.ByteStreamType.PROTECTDICT:
            if len(file_content) == 0:
                file_content = "*#06# nz".encode("UTF-8")

            if not FormatCheck.check_protect_dict_validity(file_content):
                return self.__file_format_error_respond

            # 搜索词典,作mmap映射
            if platform.system()=="Windows":
                #Windows 系统一类定义
                protectdict_map = mmap(-1,len(file_content))
            else:
                #Unix 系统一类定义
                protectdict_map = mmap(-1,len(file_content))

            protectdict_map.write(file_content)
            # 复位,不然指针指向了最后一位就读不出来了
            protectdict_map.seek(0)

            # 先删除记录着的
            if self.__client_id_config[_id].protect_dict_stream is not None:
                assert isinstance(self.__client_id_config[_id].protect_dict_stream, mmap)
                self.__client_id_config[_id].protect_dict_stream.flush()
                self.__client_id_config[_id].protect_dict_stream.close()
                del self.__client_id_config[_id].protect_dict_stream
                self.__client_id_config[_id].protect_dict_stream = None

            self.__client_id_config[_id].protect_dict_stream = protectdict_map

        return self.__ok_respond

    def predict(self,input_str,radius,_id):
        if _id not in self.__client_id_config:
            return MTA.ServicePredictRespond(predict_json_str="", status_code=self.__client_not_found_respond)
        if self.ping(_id) is not self.__ok_respond:
            return MTA.ServicePredictRespond(predict_json_str="", status_code=self.__internal_error_respond)

        assert isinstance(input_str, str)

        try:
            result_json = self._get_report_json(input_str,radius,_id)
        except Exception as e:
            return MTA.ServicePredictRespond(predict_json_str="",status_code=self.__internal_error_respond)

        return MTA.ServicePredictRespond(predict_json_str=result_json,status_code=self.__ok_respond)

    def __del__(self):
        """
        销毁所有文件
        :return:
        """
        for each in self.__client_id_config.keys():
            self.clean(each)


def run_server():
    # 先加入缓存路径
    import os
    ROOTDIR = os.getcwd()
    print(os.getcwd())
    resource_dir= os.path.join(ROOTDIR, "resources")
    cache_dir = os.path.join(ROOTDIR, "resources/_cache")
    if not os.path.exists(resource_dir):
        os.mkdir(resource_dir)
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)

    # 建立服务器
    import argparse, re

    APP_DESC = """
    --------------------------------------------------
    用于建立服务器，服务器暂时不支持训练，只支持预测功能。

    服务建立需要至少两个参数: 服务器名称和服务器的ip，端口为可选参数，默认为8080
    --------------------------------------------------
            """
    APP_EPI = """
    --------------------------------------------------
    以下代码可能对您有帮助：
    python MTA_Server.py name 127.0.0.1 [-p 8706]
    --------------------------------------------------
            """
    parser = argparse.ArgumentParser(description=APP_DESC,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=APP_EPI)

    parser.add_argument("name", help="服务器名称")
    parser.add_argument("ip", help="服务器的ip地址")
    parser.add_argument("-p", "--port", type=int, help="服务器端口号", default=8080)
    args = parser.parse_args()

    ip_regex = "^(?:(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5])|(?:[1-9][0-9]\.)|(?:[0-9]\.)){3}" \
               "(?:(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5])|(?:[1-9][0-9])|(?:[0-9]))$"
    ip_pattern = re.compile(ip_regex)

    if len(ip_pattern.findall(args.ip)):
        print("Error:ip地址格式不对！请检查")
        exit(0)
    if args.port < 0 or args.port > 65535:
        print("Error:端口号不是一个0~65535的整数")
        exit(0)

    # 这里读取命令行创建服务器，之后该进程需要被挂起
    # 高并发环境下python有全局进程锁，IO可能会成为瓶颈。

    handler = MedTextAnalyzerHandler(args.name)
    processor = MTA.Processor(handler)

    transport = TSocket.TServerSocket(host=args.ip, port=args.port)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    # 每个client应该单独开一个线程，希望不会堵上，建议不要同时超过十个
    # 所以我在java端的client请求都是即开即关，否则会线程阻塞
    server = TServer.TThreadedServer(processor, transport, tfactory, pfactory)

    print("建立服务器: 名称 %s , ip %s, 端口 %d" % (args.name, args.ip, args.port))

    try:
        server.serve()
    except (KeyboardInterrupt, SystemExit):
        print("Quitting...")
        transport.close()
        if os.path.exists(cache_dir):
            os.removedirs(cache_dir)
        if os.path.exists(resource_dir):
            os.removedirs(resource_dir)
        exit(0)

if __name__ == '__main__':
    run_server()
