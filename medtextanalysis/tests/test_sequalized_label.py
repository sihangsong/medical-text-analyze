# -*- coding: utf-8 -*-

# import os.path as os_path
#
# import pytest
# from jieba import load_userdict, initialize
#
# from medtextanalysis.sentence2report.sentence_split import feature_extraction
#
# sentence_list= [
#     u"12-18日:病毒:08-23:TORCH＋EB CMV－IgG 阳性;风疹病毒IgG 阳性; EBV-CA 阳性; 其余（－）",
#     u"【2011-10-15 22:58:00.0】患儿反应软，体温波动，呕吐明显，伴轻度腹泻，龚小慧副主任医师看过"
#     u"患儿后指示：患儿体温波动，反应差，呕吐明显，有轻微的腹泻，查体可见红疹，左侧颌下可及一豌豆"
#     u"大小淋巴结，结膜充血，口唇龟裂，杨梅舌，腹胀，患儿据按，触诊不满意，四肢无明显水肿。辅助检"
#     u"查：临检 : ESR ↑ 96mm/h;生化: ALT ↑ 122U/L;GGT ↑ 124U/L;生化: AT-3 ↓ 108mg/L;腹部B超未见"
#     u"异常。结合会儿病情考虑川崎病，为防止心脏损伤，今予12.5g丙种球蛋白抗病毒，支持治疗，考虑血制"
#     u"品风险性，告知患儿家属，家属表示理解，同意应用。患儿呕吐明显，禁食补液治疗。患儿腹部据触，腹"
#     u"部B超无明显异常，有排气排便，暂不考虑梗阻，液体维持治疗，续观患儿病情变化。"
# ]
#
#
# @pytest.fixture(params=sentence_list)
# def test_data(request):
#     '''
#     pass the unit test parameter
#     '''
#     return request.param
#
#
# def test_feature_extraction(test_data):
#     '''
#     Test if we can cut the word correctly
#     :param test_data:
#     :return:
#     '''
#     print("\nTest data : %s" % test_data)
#
#     print("Before loading userdict: ")
#
#     initialize()
#     print(feature_extraction(test_data))
#
#     print("After loading userdict: ")
#
#     load_userdict(os_path.join(os_path.dirname(__file__),'../resources/jieba_dict.txt'))
#     print(feature_extraction(test_data))

