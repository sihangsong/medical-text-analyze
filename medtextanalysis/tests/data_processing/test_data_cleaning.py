"""
data_cleaning.py 的单元测试

"""
import pytest

# 定义测试用的文档
_doc_list = [
    u"【2011-11-23 10:20:00.0】患儿@n 体温@nz 正常，咳嗽较前@d 好转@a ，皮疹及阴囊湿疹较前明显好转@d ，"
    u"结膜充血减轻，无气喘气促等，胃纳可，两便正常。    查体：神清，呼吸@nz 平稳@a ，反应可。淋巴结未及肿大，"
    u"双球结膜充血@nz ，唇红无皲裂，咽充血@nz ，双扁桃体Ⅱ度肿大，无渗出，牙龈红肿。双肺呼吸音粗，未及明显啰音。"
    u"心音有力，律齐，未及病理性杂音。腹软，肝肋下1.0cm，质软，脾肋下未及。阴囊部红色皮疹较前明显减轻，"
    u"肛周1×2cm潮红,未见明显脱屑@nz 。手指指端略轻度脱屑@nz 。四肢肌张力可。NS（－）。    "
    u"处理：患儿@n 目前@t 热平近3天，结膜充血较前@d 好转@a ，咳嗽较前@d 好转@a ，现考虑患儿@n 病情@n 好转@v ,"
    u"请示谢利剑副主任医师后准予@v 出院@v 。    出院诊断@n ：1.川崎病 2.支气管炎 3.口腔炎 4.阴囊部湿疹    "
    u"出院带药@n ：阿司匹林25mg/#×60＃ Sig: 25Mg@nz  bid      出院医嘱：1.健康宣教，专科门诊随访（周一和周四心内科专科） "
    u"               出院@v 后@p 2周复查EKG@nz 、心彩超@nz 。              2.出院半年后可打预防针。",
    u"【2011-10-15 22:58:00.0】患儿反应软，体温波动，呕吐明显，伴轻度腹泻，龚小慧副主任医师看过"
    u"患儿后指示：患儿体温波动，反应差，呕吐明显，有轻微的腹泻，查体可见红疹，左侧颌下可及一豌豆"
    u"大小淋巴结，结膜充血，口唇龟裂，杨梅舌，腹胀，患儿据按，触诊不满意，四肢无明显水肿。辅助检"
    u"查：临检 : ESR ↑ 96mm/h;生化: ALT ↑ 122U/L;GGT ↑ 124U/L;生化: AT-3 ↓ 108mg/L;腹部B超未见"
    u"异常。结合会儿病情考虑川崎病，为防止心脏损伤，今予12.5g丙种球蛋白抗病毒，支持治疗，考虑血制"
    u"品风险性，告知患儿家属，家属表示理解，同意应用。患儿呕吐明显，禁食补液治疗。患儿腹部据触，腹"
    u"部B超无明显异常，有排气排便，暂不考虑梗阻，液体维持治疗，续观患儿病情变化。"]


@pytest.fixture(params=_doc_list)
def test_data(request):
    """
    单元测试数据接口
    :param request:
    :return:
    """
    return request.param


def test_split_by_regex():
    """

    :return:
    """
    pass


def test_replace_by_regex():
    """

    :return:
    """
    pass


def test_unify_special_symbols():
    """

    :return:
    """
    pass


def test_remove_separator():
    """

    :return:
    """
    pass


def test_split_by_separator():
    """
    
    :return: 
    """
    pass


def test_remove_data_labels():
    """

    :return:
    """
    pass