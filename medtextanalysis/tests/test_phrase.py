# -*- coding: utf-8 -*-

import medtextanalysis.core.phrase as phrase
import medtextanalysis.core.word as word


def test_phrase_init():
    """
    通用测试数据赋值，不应产生异常
    """
    A = word.Word("Fuck",{"cixing": "n"})
    B = word.Comma(",",{"cixing": "n"})
    C = phrase.Phrase([A,B])
    return C


def test_phrase_property():
    """
    测试phrase类所有属性访问
    """
    sample = test_phrase_init()
    assert isinstance(sample.words,list)
    assert sample.len == 2
    assert sample.content == "Fuck,"
    assert sample.content_len == 5
    assert sample[0].content == "Fuck"


def test_phrase_append():
    """
    测试append方法
    """
    sample = test_phrase_init()
    sample.append("Shit")
    assert len(sample) == 3

    #测试子类能否通过isinstance的检查
    sample.append(word.Comma("!"))
    assert len(sample) == 4
    assert sample[-1].content == "!"


def test_phrase_prepend():
    """
    测试prepend方法
    """
    sample = test_phrase_init()
    sample.prepend("Shit")
    assert len(sample) == 3
    sample.prepend(word.Comma("!"))
    assert len(sample) == 4
    assert sample[0].content == "!"
