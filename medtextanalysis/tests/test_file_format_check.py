import pytest

from medtextanalysis.util.file_format_check import \
    check_crfmodel_validity,check_merge_dict_validity,check_search_dict_validity

string_list= [
    u"""今天 天气 真好
    我 今天 昨天 $ {%f}元""",
    u"今天 天气 真好",
    u"""爸爸&妈妈 今天 不在家 
      我 可以 玩{%eng}游戏""",
    u"""A
      
      B""",
    u"$ 哈哈 这是错误",
    u"哈哈 &这也是错误",
    u"""但是 这
    u是
    u正确的 \
    \
    """,
    ""
    ]


def test_check_crfmodel_validity():
    assert check_crfmodel_validity(string_list[0])=="IO Error"
    assert check_crfmodel_validity("readme.md") =="Format Error"


def test_check_search_dict_validity():
    assert check_search_dict_validity(string_list[0]) == True
    assert check_search_dict_validity(string_list[1]) == True
    assert check_search_dict_validity(string_list[2]) == False
    assert check_search_dict_validity(string_list[3]) == False
    assert check_search_dict_validity(string_list[4]) == False
    assert check_search_dict_validity(string_list[5]) == True
    assert check_search_dict_validity(string_list[6]) == True
    # 检查空文件是否合法
    assert check_search_dict_validity(string_list[7]) == True

def test_check_merge_dict_validity():
    assert check_merge_dict_validity(string_list[0]) == True
    assert check_merge_dict_validity(string_list[1]) == True
    assert check_merge_dict_validity(string_list[2]) == True
    assert check_merge_dict_validity(string_list[3]) == False
    assert check_merge_dict_validity(string_list[4]) == True
    assert check_merge_dict_validity(string_list[5]) == False
    assert check_merge_dict_validity(string_list[6]) == True
    # 检查空文件是否合法
    assert check_merge_dict_validity(string_list[7]) == True
