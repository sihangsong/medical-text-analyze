"""
给定单字和标注结果，还原结果为需求的report

这个部分暂时是基于语法和规则的，以后会引入学习的部分

"""
import json
import re
from itertools import product

import medtextanalysis.core.error as error
import medtextanalysis.config as conf
from medtextanalysis.util.funcs import strQ2B

class TrieTree(object):

    def __init__(self):
        self.tree = {}
        self.END = "NAN"

    def add(self, list):
        tree = self.tree

        if list[-1][0]=="@":
            endkey = list[-1][1:]
            list = list[:-1]
        else:
            endkey = "None"

        for each in list:
            if each in tree:
                tree = tree[each]
            else:
                tree[each] = {}
                tree = tree[each]

        tree[self.END] = endkey

class Report(object):
    """
    如果说序列标注是 文档——句子——短语——单词——单字 的拆解
    那么根据整个标记结果生成报告是它的一个逆向操作：
    单字——短语——句子——文档 架构的生成，需要语义模型的支持，
    这个类的作用就是接收标记好的数据然后生成对应的报告
    无论这个结果有多烂

    现阶段，我们先只做到短语的分割，以及短语和短语之间关系的提取

    """


    def __init__(self,**kwargs):
        """

        :param kwargs:
        """
        self.single_words = []
        self.label_list = []
        self.phrase_list = []
        self.phrase_label_list= []
        self.search_r = 5
        self.merge_dict=TrieTree()
        self.search_dict={}


    def set_input(self,words,labels):
        """

        :param words:
        :param labels:
        :return:
        """
        self.single_words = words
        self.label_list = labels

    def set_radius(self,radius):
        """

        :param radius:
        :return:
        """
        self.search_r = radius

    def set_search_dict(self,dict_str):
        """

        :param dict_str:
        :return:
        """
        if dict_str=="*#06#":
            return
        dict_str = strQ2B(dict_str)
        assert isinstance(dict_str, str)

        lines = dict_str.split("\n")
        sp = re.compile(u"\s+")

        for each_line in lines:
            each_line = sp.sub(" ", each_line)

            has_split_showed_up = False

            if each_line == " ":
                continue

            for (index, each) in enumerate(each_line.split(" ")):
                if len(each)==0:
                    continue
                if index == 0:
                    key = each
                    self.search_dict[key] ={
                        "forward": [],
                        "back" : []
                    }
                    continue
                if each == "$":
                    has_split_showed_up = True
                    continue
                if has_split_showed_up:
                    self.search_dict[key]["back"].append(each)
                else:
                    self.search_dict[key]["forward"].append(each)


    def set_merge_dict(self,dict_str):
        """

        :param dict_str:
        :return:
        """
        if dict_str=="*#06#":
            return

        dict_str = strQ2B(dict_str)
        sp = re.compile(u"\s+")

        lines = dict_str.split('\n')

        for each_line in lines:
            each_line = sp.sub(" ", each_line)
            if each_line == " ":
                continue

            each_line_list= []

            phrases = each_line.split(" ")
            for (index, each) in enumerate(phrases):
                if len(each)==0:
                    continue
                if "&" in each:
                    each_line_list.append(each.split("&"))
                else:
                    each_line_list.append([each])
            # python 作笛卡儿积
            each_line_list = product(*each_line_list)
            #print(list(each_line_list))
            for everything in each_line_list:
                #print(list(everything))
                self.merge_dict.add(list(everything))

        #print(self.merge_dict.tree)

    def predict_label_string(self):
        """
        转化为标记数据
        :return:
        """
        str_list= []
        check_label_list=list(conf.GlobalVar.SYMBOL_LABEL_DICT.values()) + ["date","time","num","item","complex_eng","unit"]
        for word,label in zip(self.phrase_list,self.phrase_label_list):
            if label in check_label_list:
                # 无需标注的词语
                str_list.append(word)
            else:
                # 需要添加标注的词语
                str_list.append("".join([word,"@",label]))
        return " ".join(str_list)

    def _form_result_str(self, cwords, mwords):
        ans = ""
        for i in range(len(cwords)):
            ans += "({} : {}); ".format(cwords[i], mwords[i])
        return ans

    def generate_json_report(self,words=None, labels=None, complex_format=False):
        """

        :param words:
        :param labels:
        :return:
        """
        if words is None:
            words= self.single_words
        if labels is None:
            labels= self.label_list
        self.set_input(words,labels)
        self.merge_BMSE_label()
        #print(self.phrase_list)
        #print(self.phrase_label_list)
        c,m = self.relation_extraction()

        if self.first_value_date:
            result = json.dumps({
                "time" : self.center_word_templates[0]["key"],
                "report" : self.center_word_templates[1:],
                "formatted_string": self._form_result_str(c,m),
                "predict_labeling": self.predict_label_string()

            }, ensure_ascii=False, indent=2)
        else:
            result = json.dumps({
                "time" : "",
                "report": self.center_word_templates,
                "formatted_string": self._form_result_str(c, m),
                "predict_labeling": self.predict_label_string()
            },ensure_ascii=False, indent=2)
        return result.encode("UTF-8").decode("UTF-8")


    def generate_report(self,words=None,labels=None):
        """

        :param words:
        :param labels:
        :return:
        """
        if words is None:
            words= self.single_words
        if labels is None:
            labels= self.label_list
        self.set_input(words,labels)
        self.merge_BMSE_label()

        def _form_result_str(cwords, mwords):
            ans = ""
            for i in range(len(cwords)):
                ans += "({} : {}); ".format(cwords[i], mwords[i])
            return ans

        c,m = self.relation_extraction()
        return _form_result_str(c,m)

    def _add_phrase(self, phrase, label):
        self.phrase_list.append(phrase)
        self.phrase_label_list.append(label)

    def merge_BMSE_label(self):
        """
        第一步，还原 BMSE标签到短语level
        :return:
        """
        phrase=""
        phrase_label="?"

        phrases=[]
        labels=[]

        for word,label in zip(self.single_words,self.label_list):
            if label[0:2]=="b-":
                # b标签，上一个短语强制中断
                # 同时以b标签的标注作为短语的标签
                if len(phrase)>0:
                    phrases.append(phrase)
                    labels.append(phrase_label)
                phrase= word
                phrase_label= label[2:]
            elif label[0:2]=="m-":
                # m标签，短语延续
                phrase += word
                if phrase_label == "?":
                    phrase_label = label[2:]
                #if phrase_label != label[2:]:
                #    print("Strange!")

            elif label[0:2]=="e-":
                # e标签，短语正常结束
                phrase += word
                #if phrase_label!= label[2:]:
                #    print("Strange!")
                phrases.append(phrase)
                labels.append(phrase_label)
                phrase=""
                phrase_label="?"

            elif label[0:2]=="s-":
                # s标签，上一个短语强制中断且这一个短语开始了
                if len(phrase)>0:
                    phrases.append(phrase)
                    labels.append(label)
                phrase= word
                phrase_label= label[2:]
                phrases.append(phrase)
                labels.append(phrase_label)
                phrase = ""
                phrase_label = "?"
            else:
                raise error.UnknownError("Why we have a label out of BMSE scope here?\n")

        if(len(phrase)>0):
            # 标签不是100%正确的，所以可能会出现没结束的情况，需要特殊判断一下
            phrases.append(phrase)
            labels.append(phrase_label)

        # update on 2017/11/29
        # 根据合并词典合并数据
        # 合并数字+单位短语为item
        i = 0
        length = len(phrases)

        tree_node= self.merge_dict.tree
        while i < length:
            if phrases[i] in tree_node.keys():
                j=0
                while i+j< length and phrases[i+j] in tree_node.keys():
                    tree_node= tree_node[phrases[i+j]]
                    j += 1
                if self.merge_dict.END in tree_node:
                    big_phrase = "".join(phrases[i:i+j])
                    big_label= tree_node[self.merge_dict.END]
                    if big_label == "None":
                        big_label = labels[i+j-1]
                    self._add_phrase(big_phrase,big_label)
                    i += j
                else:
                    i += 1
            elif labels[i] in ["num","q"]:
                if i+1 < length and labels[i+1] == "unit":
                    self._add_phrase(phrases[i]+phrases[i+1], "item")
                    i += 2
                else:
                    self._add_phrase(phrases[i], labels[i])
                    i += 1
            else:
                self._add_phrase(phrases[i],labels[i])
                i += 1



    def _in_search_modifier(self,string,label,match_list):
        """

        :param string:
        :param match_list:
        :return:
        """
        for each in match_list:
            if string == each:
                return True
            if "{" in each:
                pattern = re.compile(u"\{%[A-Za-z]+\}")
                tag = pattern.split(each)


    def relation_extraction(self):
        """
        第二步，根据短语和短语之间的关系，还原
        主要是在一定范围内搜索

        :return:
        """


        words = self.phrase_list
        labels = self.phrase_label_list

        i = 0
        length = len(words)

        def __is_central(label):
            return label in ["nz", "complex-eng", "eng"]

        def __is_separator(label):
            return label in ["comma", "colon","semicolon","stop","date","time"]

        def __is_modifier(label):
            return label in ["a","ad","an","az","d","dz","v","q","f", "t","z","rz","m"
                             "plus","minus","down","up","item","num","unit"]


        center = []
        modify = []
        self.center_word_templates=[]
        used_word = [False]*length
        self.first_value_date = False
        while i < length:
            # 特判日期和时间
            if labels[i] == "date" and not used_word[i]:
                if i == 0:
                    self.first_value_date= True
                if i < length-1:
                    if labels[i+1] == "time":
                        center_noun = words[i]+" "+words[i+1]
                        used_word[i + 1] = True
                    else:
                        center_noun = words[i]
                else:
                    center_noun=words[i]
                center.append(center_noun)
                modifications = " $ :"
                modify.append(modifications)
                self.center_word_templates.append(
                    {
                        "key": center_noun,
                        "forward": [],
                        "back": [":"],
                        #"report_string": "({} : {}); ".format(center_noun, modifications)
                    }
                )
                used_word[i] = True
                i += 1
                continue
            if labels[i] == "time" and not used_word[i]:
                if i < length-1:
                    if labels[i+1] == "time":
                        center_noun = words[i]+" "+words[i+1]
                        used_word[i+1] = True
                    else:
                        center_noun = words[i]
                else:
                    center_noun=words[i]
                center.append(center_noun)
                modifications= " $ :"
                modify.append(modifications)
                self.center_word_templates.append(
                    {
                        "key" : center_noun,
                        "forward": [],
                        "back": [":"],
                        #"report_string": "({} : {}); ".format(center_noun, modifications)
                    }
                )
                used_word[i] = True
                i += 1
                continue
            # 正常的词
            if __is_central(labels[i]) or words[i] in self.search_dict.keys():
                if words[i] in self.search_dict.keys():
                    is_force_search = True
                    search_key = words[i]
                else:
                    is_force_search = False
                    search_key = None
                if not used_word[i] or is_force_search:
                    used_word[i] = True
                    center_noun= words[i]
                    # 我们遇到了一个介词，试着扩展中心语
                    if i < length-2:
                        if labels[i+1]=="c" or labels[i+1]=="cc":
                            # 小心标注错误带来的异常，判断是否真的为 nz.+c.+nz.结构
                            if  __is_central(labels[i+2]):
                                j = i-1
                                center_noun = words[i]+words[i+1]+words[i+2]
                                used_word[i+1] = True
                                used_word[i+2] = True
                                i += 2
                            else:
                                j = i-1
                        else:
                            j = i-1
                    else:
                        j = i-1
                    template = {
                        "key": center_noun
                    }
                    modifications= ""
                    is_second_noun = False
                    is_first_a = False

                    # 前向搜索
                    good_word= []
                    break_barrier = False
                    # force_search 表明无视分隔符等语法规则强行先匹配
                    while j>0 and i-j <= self.search_r:
                        if is_force_search:
                            #print(self.search_dict[search_key])
                            if self._is_in_search_dict(words[j],self.search_dict[search_key]["forward"]):
                                if __is_separator(labels[j]) and labels[j] != "colon":
                                    break
                                used_word[j] = True
                                good_word= [words[j]]+good_word

                                j -= 1
                                continue
                            elif break_barrier:
                                j -= 1
                                continue
                        if used_word[j]:
                            if is_force_search:
                                pass
                            else:
                                break
                        if __is_separator(labels[j]):
                            if is_force_search and labels[j]=="colon":
                                break_barrier = True
                            else:
                                break
                        elif __is_central(labels[j]):
                            if is_second_noun:
                                if is_force_search:
                                    break_barrier = True
                                else:
                                    break
                            is_second_noun= True
                            good_word= [words[j]]+good_word
                            used_word[j] = True
                        elif __is_modifier(labels[j]):
                            good_word= [words[j]]+good_word
                            used_word[j] = True
                        else:
                            if is_force_search:
                                break_barrier = True
                            else:
                                break
                        j-=1
                    template["forward"] = good_word
                    modifications += " ".join(good_word) + " $ "

                    j=i+1
                    is_second_noun= False
                    is_first_a = False

                    good_word=[]
                    # 后向搜索
                    break_barrier = False
                    while j < length and j-i <= self.search_r:
                        if is_force_search:
                            if self._is_in_search_dict(words[j], self.search_dict[search_key]["back"]):
                                good_word.append(words[j])
                                # 只有一种情况中止暴力匹配: 理论上一个指标只能有一个数值
                                used_word[j] = True
                                if labels[j]=="item" or labels[j]=="time" or (__is_separator(labels[j]) and labels[j] != "colon"):
                                    break
                                j += 1
                                continue
                            elif break_barrier:
                                j += 1
                                continue
                        if j+1 < length and labels[j+1]=="colon":
                            if is_force_search:
                                break_barrier = True
                            else:
                                break
                        if __is_separator(labels[j]) and labels[j] != "colon":
                            break
                        if labels[j] == "colon":
                            good_word.append(words[j])
                            used_word[j] = True
                            if is_force_search:
                                break_barrier = True
                            else:
                                break
                        elif __is_central(labels[j]):
                            if is_second_noun:
                                if is_force_search:
                                    break_barrier = True
                                else:
                                    break
                            is_second_noun = True
                            good_word.append(words[j])
                            used_word[j] = True
                        elif __is_modifier(labels[j]):
                            good_word.append(words[j])
                            used_word[j] = True
                            if labels[j] == "item" or labels[j] == "time":
                                break
                        else:
                            pass
                        j += 1
                    modifications += " ".join(good_word)

                    template["back"] = good_word
                    #template["report_string"] = "({} : {}); ".format(center_noun, modifications)

                    center.append(center_noun)
                    modify.append(modifications)

                    self.center_word_templates.append(template)

            i=i+1

        return center,modify

    def _is_in_search_dict(self,string,lookup_list):

        if len(lookup_list)==0:
            return False
        if string in lookup_list:
            return True

        pattern = re.compile(u"\{%[A-Za-z]+\}")
        # complex pattern search
        # 这里有个潜在bug，标点会返回true，有空修复一下
        for each in lookup_list:
            symbols = pattern.findall(each)
            if len(symbols) == 0:
                continue
            # print(symbols)
            symbols.append("")
            str_idx = 0
            normal_word_list = pattern.split(each)
            for index, component in enumerate(normal_word_list):
                # print(str_idx)
                if len(component) > 0:
                    # 文字部分
                    part = string[str_idx: str_idx + len(component)]
                    if part != component:
                        return False
                    str_idx += len(component)
                # 特殊部分
                if symbols[index] == "{%d}":
                    int_pattern = conf.GlobalVar.PREDEFINED_REGEX["integer"]
                    int_pattern = re.compile(int_pattern)
                    match = int_pattern.match(string, pos=str_idx)
                    # print(match.group())
                    if match is None or match.pos != str_idx:
                        return False
                    str_idx += len(match.group())
                elif symbols[index] == "{%f}":
                    float_pattern = conf.GlobalVar.PREDEFINED_REGEX["number"]
                    float_pattern = re.compile(float_pattern)
                    match = float_pattern.match(string, pos=str_idx)
                    if match is None or match.pos != str_idx:
                        return False
                    str_idx += len(match.group())
                elif symbols[index] != "":
                    return False

        return True


