"""

封装的预测标注算法

接口接收一个装有特征模板的序列标注类的列表 (list of SeqLabel),目标是得到一个标签预测的结果，
最后算法返回一个单字列表和标注列表，用于下一步处理并提取报告。

这个本质上是对python-crfsuite 中tagger类的封装，如果我们有特殊的需求，就在这个类里面实现

"""
from pycrfsuite import Tagger as pycrfTagger

import medtextanalysis.core.error as error
import medtextanalysis.core.sequalized_label as _seqlabel


class CRFTagger(object):
    """
    预测类，用于使用训练好的模型预测标签

    注意输入也必须是特征模板类，只不过是未标记的

    """
    @property
    def tagger(self):
        return self.__tagger

    @property
    def tag_input(self):
        return "".join(self.__last_tag_content)

    @property
    def tag_result(self):
        return self.__last_tag_result

    # 这个属性就是把单字和结果变成了一个字典一样的输出 ("A":"B")
    @property
    def tag_bind_result(self):
        return self.__last_tag_bind_result

    @property
    def has_import_model(self):
        return self.__has_import_model

    def __init__(self, model_filename=None, **kwargs):
        """

        :param model_filename:
        :param kwargs:
        """
        # 创建容器
        self.__tagger = pycrfTagger()
        self.__has_import_model = False
        self.__has_set_tagger = False

        # 是否读入了模型文件
        if model_filename is not None:
            self.open(model_filename)

        # 标记是否把待标记数据读进来了

        self.__last_tag_content = ""
        self.__last_tag_result = ""
        self.__last_tag_bind_result = list()

    def open(self, model_filename):
        """
        更换模型文件
        :param model_filename:
        :return:
        """
        # 理论上不可能的情况

        self.__tagger.open(model_filename)
        self.__has_import_model = True

    def set_tagger(self, template):
        """
        设定预测器的输入
        可以先赋值，之后调用tag方法等其它方法就不需要再赋值了
        :return:
        """

        if not isinstance(template, _seqlabel.SeqLabel):
            raise error.TypeError(type(template),"SeqLabel Class")

        self.__tagger.set(template.model)
        self.__has_set_tagger = True
        self.__last_tag_content = template.word_contents

    def tag(self, template=None, result_format="plain"):
        """
        做预测，并存储标签


        :param template:
        :param result_format: 参数返回格式选项
                    "plain" : 只有结果列表
                    "bind"  : 返回类似字典的字符串
                    "dual"  : 分别返回结果和对应的单字列表
                    "tuple" : 返回二元组(待实现)
                    "dict"  : 返回字典(待实现)
        :return: 默认返回的是只含标签的数据，可通过参数控制返回内容
        """

        if template is None and not self.__has_set_tagger:
            try:
                raise error.InputError("template")
            except error.InputError as e:
                print("Error at label predict since no template is setting for tagger!")
            return

        # 防止有人不载入模型就调用预测方法，抛出错误信息
        if not self.__has_import_model:
            try:
                raise error.UnknownError("Try to tag result before the model has imported!\n"
                                         "You should first call open method to import a crfmodel!")
            except error.UnknownError as e:
                print(e.err_msg)
                return

        if template is None:
            self.__last_tag_result = self.__tagger.tag()
        else:
            if not isinstance(template, _seqlabel.SeqLabel):
                raise error.TypeError(type(template), "SeqLabel Class")
            self.set_tagger(template)
            self.__last_tag_result = self.__tagger.tag()

        self.__last_tag_bind_result = []
        for word, label in zip(self.__last_tag_content,self.__last_tag_result):
            self.__last_tag_bind_result.append("".join([word," : ",label]))

        # 原则：常用的可以存储起来，不常用的可以现构造
        if result_format=="plain":
            return self.__last_tag_result
        elif result_format == "bind":
            return self.__last_tag_bind_result
        elif result_format == "dual":
            return self.__last_tag_result, self.__last_tag_content
        else:
            return self.__last_tag_result
