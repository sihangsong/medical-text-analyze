"""

封装的训练算法

接口接收一个装有标记好特征模板的序列标注类的列表 (list of SeqLabel),以及训练参数，
目标是输出训练模型或者完成训练步骤

现阶段使用的python-crfsuite 会将训练模型保存在一个二进制文件中。该程序会将改二进制文件默认保存到
resources/_cache 文件夹下作为临时模型变量，然后供标记/测试算法调用

"""
from pycrfsuite import Trainer as pycrfTrainer

import os
import configparser
import medtextanalysis.config as conf
import medtextanalysis.core.error as error
import medtextanalysis.core.sequalized_label as _seqlabel

#这一段可粘贴至其他程序，来实现通过ini文件取参数值
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
configPath=os.path.join(rootPath,"config.ini")
config=configparser.ConfigParser()
config.readfp(open(configPath))
c1=config.getfloat("train","c1")
c2=config.getfloat("train","c2")
max_ir=config.getint("train","max_iterations")
feature_trans=config.getboolean("train","feature.possible_transitions")
feature_minfreq=config.getfloat("train","feature.minfreq")

class CRFTrainer(object):
    """
    训练类，用于生成CRF训练模板结果

    """
    @property
    def trainer(self):
        return self.__trainer

    @property
    def has_trained(self):
        return self.__is_train

    @property
    def dump_model_abspath(self):
        return self.__dump_model_abspath


    def __init__(self, template_list=[], **kwargs):
        """

        :param template_list:
        :param kwargs:
        """
        self.__is_train = False
        self.__dump_model_abspath = ""

        if not isinstance(template_list, list):
            raise error.TypeError(type(template_list), "list")

        # 创建容器
        self.__trainer = pycrfTrainer(verbose=kwargs.get("verbose","True"))

        # TODO: (wy) 这里可以改成可变参数
        self.__trainer.set_params({
            "c1": c1,
            "c2": c2,
            "max_iterations": max_ir,
            "feature.possible_transitions": feature_trans,
            "feature.minfreq": feature_minfreq

        })

        # 把内容添加到训练器中
        for each in template_list:
            # 检查实例的类型
            if not isinstance(each, _seqlabel.SeqLabel):
                raise error.TypeError(type(each), "SeqLabel Class")
            self.__trainer.append(each.model, each.label)

    def train(self,model_cache_filename=None):
        """
        执行训练过程
        :return:
        """
        if model_cache_filename is None:
            model_cache_filename = conf.Config.temp_crf_model_filename
        self.__trainer.train(model_cache_filename)
        self.__dump_model_abspath = model_cache_filename
        self.__is_train = True

    def set_train_param(self,**kwargs):
        """
        调整训练参数
        :param kwargs:
        :return:
        """
        # 等待日后和可变参数一起实现
        pass

    def append(self, template):
        """
        追加一个模板

        :param template:
        :return:
        """
        if not isinstance(template, _seqlabel.SeqLabel):
            raise error.TypeError(type(template), "SeqLabel Class")
        self.__trainer.append(template.model, template.label)
