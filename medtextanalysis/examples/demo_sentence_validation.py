# -*- coding: utf-8 -*-
"""
This is a test file of sentence_validation.py, showing the functions and variables of Class 'nlp'
Running time：about 20s

！！Contains an output！！
"""
import os

import medtextanalysis.data_processing.sentence_validation as h

source=os.path.dirname(os.path.dirname(__file__))+"/resources/test_input.xls" #测试文本的地址
out=os.path.dirname(__file__)+"/demo_sentence_validation_output.xls"       #文件输出地址

test=h.nlp(source,1)  #定义类
#test.dict   #查看二元组结果，内容过长，不建议直接在python中打印
print(test.property_d)        #查看词性字典
print(test.error)            #查看错误（只包含空内容空词性和带@的）
test.output(out)      #将结果输出到指定路径