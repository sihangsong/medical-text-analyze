# coding=utf-8


# 修改这里的路径换输入
__fileAdd__ = 'D:\\Test\\medtextanalysis\\resources\\test_input_crfresult.xlsx'


determinist_map = {
    "发热": ["有", "无", "天"],
    "体温": ["正常", "平稳", "不稳", "不平稳", "热峰", "波动"],
    "丙球": ["给予", "治疗", "今", "昨", "继续", "停用", "输注", "支持", "输入"],
    "丙种球蛋白": ["给予", "治疗", "今", "昨", "继续", "停用", "输注", "支持", "输入"],
    "心超": [],
    "左冠状动脉": ["cm", "扩张", "正常"],
    "右冠状动脉": ["cm", "扩张", "正常"],
    "左、右冠状动脉": ["增宽", "增宽"],
    "左右冠脉": ["未见异常", "增宽"],
    "左右冠状动脉": ["瘤样扩张"],
    "心包": ["积液", "少量"],
    "左心室": ["收缩", "正常"],
    "心内结构": ["正常", "大致正常", "未见异常"],
    "心脏结构": ["正常", "大致正常", "未见异常"],
    "冠状动脉": ["未见扩张"],
    "二尖瓣": ["返流"],
    "心电图": ["正常"],
    "T波": ["略钝"],
    "窦性心动": ["过速", "过缓", "不齐"],
    "P-R": ["间期", "延长"],
    "四肢末梢": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "四肢": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "双手": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "足": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "指端": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "指趾": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "趾": ["红肿", "肿胀", "蜕皮", "脱屑", "斑丘疹", "皮疹"],
    "皮疹": ["天"],
    "眼结膜": ["充血", "天"],
    "唇": ["红", "皲裂"],
    "口周": ["红", "皲裂"],
    "舌乳头": ["增粗", "明显"],
    "杨梅舌": [],
    "扁桃体": ["红", "肿", "红肿"],
    "咽": ["红", "肿", "充血"],
    "淋巴结": ["肿大"],
    "肛周": ["蜕皮", "脱屑", "红", "疹"],
    "躯干": ["疹"],
    "全身": ["疹"],
    "疫苗接种处": ["溃烂"],
    "出院诊断": ["川崎病"],
    "面部": ["红色", "斑丘疹", "压之褪色"],
    "四肢": ["红色", "斑丘疹"],
    "手心": ["潮红"],
    "足底": ["潮红"],
    "皮肤": ["潮红"]
}

import pandas as pd

predicted_sheet = pd.read_excel(__fileAdd__)

reports = predicted_sheet[u"Report"]
origin = predicted_sheet[u"Origin"]

determinist_keys = determinist_map.keys()


# 判别字典对比原文，筛捡出存在的。
def get_compare_dic(origin):
    compare_dic = {}
    for key in determinist_keys:
        index = origin.find(key)
        # 确定key在原文中的位置后
        ##计算key对应value搜索范围
        if index != -1:
            origin_len = len(origin)
            if index < 8:
                forward_range = 0
            else:
                forward_range = index - 7

            after_range = origin_len - index
            if after_range < 9:
                after_range = origin_len
            else:
                after_range = index + 7 + len(key)

            # 检索子串
            search_range_test = origin[forward_range:after_range]
            # print(search_range_test)
            compare_tmp = []
            compare_values = determinist_map[key]
            # 筛捡原文存在的参考值
            for compare_value in compare_values:
                if compare_value in search_range_test:
                    compare_tmp.append(compare_value)

            if compare_dic.get(key, None) is None:
                compare_dic[key] = compare_tmp
            else:
                compare_dic[key].extend(compare_tmp)
    return compare_dic


## 解析结构化的内容
def parse_report_list(units):
    parsed_dic = {}
    for j, unit in enumerate(units):
        unit= unit.lstrip(" ").rstrip(" ")
        if len(unit) < 2:
            continue
        if not ('$' in unit):
            continue
        # print('uni',unit)
        dued = unit.replace("(", "").replace(")", "").split(":")
        if len(dued) >= 3:
            print('parse_report_list error:', unit)
            continue
        #print(dued)
        key = dued[0].strip()
        forward = dued[1].split('$')[0].strip()
        back = dued[1].split('$')[1].strip()
        # print(i,j,key,forward,back)
        if parsed_dic.get(key, None) is None:
            parsed_dic[key] = [forward, back]
        else:
            parsed_dic[key].append(forward)
            parsed_dic[key].append(back)

    return parsed_dic

if __name__=="__main__":
    key_pos = key_nav = value_pos = value_nav = 0

    for i, report in enumerate(reports):
        if not isinstance(report, str): continue
        if len(report) < 2: continue
        units = report.split(';')
        #print(units)

        # 判别字典对比原文，筛捡出原文中存在的。
        compare_dic = get_compare_dic(origin[i])
        #print("compare_dic", compare_dic)

        ## 解析结构化后的内容
        parsed_dic = parse_report_list(units)
        #print("parsed_dic", parsed_dic)

        ##逐个对比原文中存在的判别字典项目
        for compare_key in compare_dic.keys():
            key_pos_keeper = None
            value_pos_keeper = None
            for parsed_key in parsed_dic.keys():##逐个对比原文中存在的判别字典项目的key
                if compare_key in parsed_key: ##key正确存在时
                    key_pos_keeper = (parsed_key,compare_key)

                    value_pos_keeper = len(compare_dic[compare_key]) ## 原文中没有判别字典中对应的value时，跳过，不计数

                    for cv in compare_dic[compare_key]: ##key正确存在时，对比应该存在的value
                        for pv in parsed_dic[parsed_key]:
                            if len(pv) == 0:continue
                            if cv in pv or pv in cv:
                                value_pos_keeper = (pv, cv) ##只要结构化结构中存在一个正确解，就停止查找，后边会记为positive
                                break
                        # if value_pos_keeper != None:
                        #     break

            ####根据对比结果计数，结构化中找到key，没找到key，
            if key_pos_keeper != None:
                key_pos += 1
            else:
                key_nav += 1
                print("loss key:",i,compare_key)

            if key_pos_keeper != None :
                if isinstance(value_pos_keeper, tuple):
                    value_pos += 1
                    # print(value_pos_keeper)
                    # print("have value:", i, key_pos_keeper, parsed_dic[key_pos_keeper[0]], compare_dic[key_pos_keeper[1]])
                elif value_pos_keeper == 0:
                    value_pos += 0
                    # print(value_pos_keeper)
                    # print("no value:", i, key_pos_keeper, parsed_dic[key_pos_keeper[0]], compare_dic[key_pos_keeper[1]])
                else:
                    value_nav += 1
                    # print(value_pos_keeper)
                    print("loss value:", i, key_pos_keeper,parsed_dic[key_pos_keeper[0]], compare_dic[key_pos_keeper[1]])

    print("key_pos:", key_pos, "  key_nav:", key_nav, "  value_pos:", value_pos, "  value_nav:", value_nav)
    print("key_pos_rate", float(key_pos)/float(key_pos+key_nav))
    print("value_pos_rate", float(value_pos)/float(value_pos+value_nav))

