"""
正确的食用方式

"""
# import pycrfsuite
import os

import pandas as pd
import time
import medtextanalysis.algorithm.report_generator as ReportGenerator
import medtextanalysis.algorithm.tagger as Tagger
import medtextanalysis.algorithm.trainer as Trainer
import medtextanalysis.core.document as document
import medtextanalysis.core.sequalized_label as seqlabel
from medtextanalysis.data_processing.protect_labeling import add_label_by_protect_dict
from medtextanalysis.util.functional_wrapper import timer
from medtextanalysis.core.IO.file_reader import FileReader
from medtextanalysis.core.IO.excel_reader import ExcelReader
from medtextanalysis.core.IO.protect_dict_reader import ProtectDictReader
import medtextanalysis.util.file_format_check as FormatCheck
from medtextanalysis.util.evalutation import enumerate_result

__author__=["panzer_wy","ssh"]

@timer
def test_all_test_samples(input_filename,
                          output_filename,
                          tagger,
                          search_dict_filename=None,
                          merge_dict_filename=None,
                          protect_dict_filename=None,
                          ):
    """
    测试指定文件的所有字符串
    假定接口为"内容“列，这个就不检查文件格式和读取了
    :param input_filename:
    :param output_filename:
    :param tagger:
    :param search_dict_filename:
    :param merge_dict_filename:
    :param protect_dict_filename:
    :return:
    """
    input_sheet = pd.read_excel(input_filename)

    reports= []
    predicts=[]

    total = len(input_sheet[u"内容"])
    count = 0
    percent = 0


    if protect_dict_filename is not None:
        X = ProtectDictReader(protect_dict_filename)
        protect_dict = X.parse_protect_dict()
    else:
        protect_dict = None

    #rootdir=os.path.dirname(__file__)

    for each in input_sheet[u"内容"]:
        if not isinstance(each,str):
            reports.append("")
            predicts.append("")
            continue

        if count>=total/100:
            print(".",end="")
            count= 0
            percent +=1
            if percent % 20 ==0:
                print(str(percent)+"% complete")
        count += 1

        if protect_dict is not None:
            each = add_label_by_protect_dict(each,protect_dict)

        A = document.LabeledDocument(each, keep_separator=True)
        B = seqlabel.SeqLabel(A, reserve_unknown=True)
        label, word = tagger.tag(B, result_format="dual")
        C = ReportGenerator.Report()
        C.set_input(word, label)

        
        if search_dict_filename is not None:
            search_dict_fhandler = FileReader(search_dict_filename)
            C.set_search_dict(search_dict_fhandler.get_content())
        if merge_dict_filename is not None:
            merge_dict_fhandler = FileReader(merge_dict_filename)
            C.set_merge_dict(merge_dict_fhandler.get_content())
        report_str = C.generate_report()
        predict_label_str = C.predict_label_string()
        reports.append(report_str)
        predicts.append(predict_label_str)


    save = pd.DataFrame({'Origin': input_sheet[u"内容"], 'Report': reports, 'Predict': predicts})
    save.to_excel(output_filename,index=True,encoding='utf-8',sheet_name='result')

def train_data(fileAdd, protect_dict_filename=None, time_stamp=""):
    # 测试文本的地址
    source=fileAdd
    add=os.path.split(fileAdd)[0]
    file_lname=os.path.split(fileAdd)[1]
    file_sname=os.path.splitext(file_lname)[0]
    # 文件输出地址
    out=os.path.join(add,file_sname+".crfmodel")
    tmpout=out
    if os.path.exists(tmpout):
        tmpout=os.path.join(add,file_sname+time_stamp+".crfmodel")

    # 读取有标定批次的内容
    table= pd.read_excel(source)

    docs = table[u"内容"]
    
    # 确认是内容数目
    print("该文件下有%d条内容" % len(docs))
    
    test = Trainer.CRFTrainer()
    cnt=0

    if protect_dict_filename is not None:
        X = ProtectDictReader(protect_dict_filename)
        protect_dict = X.parse_protect_dict()
    else:
        protect_dict = None

    for each_line in docs:
        if protect_dict is not None:
            each_line = add_label_by_protect_dict(each_line, protect_dict)
        # 带标点的情形下清洗数据
        A=document.LabeledDocument(each_line,keep_separator=True)
        # 程序中提取标注时失败的案例，不一定是没标注好，有些很显然是程序BUG，比如中文单位
#==============================================================================
#         flag = A.is_unknown_label_exists()
#         if not flag:
#             print("line : %s" % docs.index[cnt])
#             print(each_line)
#==============================================================================
        # 生成特征模板和标签
        B=seqlabel.SeqLabel(A)
        # print(B.label_contents)
        # 加入训练集
        test.append(B)
        cnt = cnt + 1
    # 训练
    print("数据正在训练，该步骤需要一定时间")
    print("请耐心等候......")

    test.train(tmpout)

    if not tmpout==out:
        print("输出模型文件已存在，输出将自动添加前缀")
    print("数据训练已经完成，模型已输出至%s" % tmpout)


def predict_data(modelAdd,
                 fileAdd,
                 search_dict_filename=None,
                 merge_dict_filename=None,
                 protect_dict_filename=None,
                 time_stamp=""):
    """

    :param modelAdd:
    :param fileAdd:
    :param search_dict_filename:
    :param merge_dict_filename:
    :param protect_dict_filename:
    :param time_stamp:
    :return:
    """
    print("模型文件格式检查：",FormatCheck.check_crfmodel_validity(modelAdd))
    if FormatCheck.check_crfmodel_validity(modelAdd) is not "OK":
        print("Error: 模型文件有误，请重新输入")
        return
    A = ExcelReader(fileAdd)
    print("表格文件格式检查：",A.is_file_readable())
    if A.is_file_readable():
        print("Error: 输入的预测文件格式不正确，请检查")

    print("相关词典文件如果格式有误，系统会自动忽略所有的错误行，建议确认格式无误后使用扩展词典")

    source=modelAdd
    add=os.path.split(fileAdd)[0]
    file_lname=os.path.split(fileAdd)[1]
    file_sname=os.path.splitext(file_lname)[0]
    file_last=os.path.splitext(file_lname)[1]
    out=os.path.join(add, file_sname+"_crfresult.xlsx")
    tmpout = out
    print(tmpout)
    if os.path.exists(tmpout):
        tmpout = os.path.join(add,file_sname+"_crfresult"+time_stamp+".xlsx")
    print(tmpout)
    tagger = Tagger.CRFTagger()
    tagger.open(source)
    test_all_test_samples(fileAdd, tmpout, tagger, search_dict_filename, merge_dict_filename)
    if not tmpout==out:
        print("输出模型文件已存在，输出将自动添加前缀")
    print("数据处理已经完成，结果已输出至%s" % tmpout)

    print("\n准确率测试")
    enumerate_result(tmpout)
    print("测试结束")

#==============================================================================
#     
#     # 这个未标记数据在训练集里
#     test_doc= u"【2009-12-29 10:04:00.0】患儿体温正常,少咳,无吐泻，胃纳差，二便无殊。    P.E：神情，气平，左下颌有肿胀，"\
#         u"可触及数枚肿大淋巴结，双眼球结膜已无明显充血，口唇暗红有皲裂，舌乳头增粗较前减轻，咽部红肿，双肺呼吸音粗，"\
#         u"未及罗音，心音有力，律齐，无杂音，腹软，肝脾未及肿大，四肢活动可，神经系统未见异常。肢端明显蜕皮。    "\
#         u"Lab：2009-12-28: ALT 15U/L;AST ↑ 47U/L;    处理：黄玉娟主治医师查看患儿，患儿体温已恢复正常，咳嗽减轻"\
#         u"，病情好转中，予以出院。    出院诊断：川崎病；窦性心动过缓；支气管肺炎    出院带药：肠溶阿司匹林×1瓶 "\
#         u"25mg  Bid  po；肝泰乐×1瓶  1粒  tid  po；潘生丁×1盒 25mg bid po；希刻劳×1盒 1包 bid po；敌咳×1瓶 "\
#         u"2ml tid po"
#     
#     # 这个没有标定批次，肯定不在训练集里
#     test_doc_unknown = u"【2010-03-09 10:02:00.0】患儿体温正常，偶咳嗽，无气喘气促，胃纳可，两便正常。  " \
#                        u"查体：意识清，精神佳，全身无皮疹，球结膜无充血，浅表淋巴结未及肿大，口唇皲裂好转，" \
#                        u"杨梅舌（－），指端褪皮（＋），肛周褪皮（-）。双肺呼吸音粗，未及干湿罗音，心律齐，" \
#                        u"心音有力，各瓣膜区无杂音，腹软无压痛，肝脾肋下未及肿大，四肢活动可，四肢肌张力可，" \
#                        u"病理征（-）。    辅检：03-08: WBC ↑ 12.1*10^9/L;HCG 110g/L;PLT ↑ 520*10^9/L;NE%" \
#                        u" 55%;LY% 24;CRP 5mg/L;临03-08: ESR ↑ 60mm/h;03-08: DBIL ↑ 4umol/L;TBIL 11umol/L" \
#                        u";ALT 25U/L;AST 39U/L;ALB ↓ 33g/L;GLOB ↑ 46g/L;    处理：患儿目前一般情况可，" \
#                        u"请示上级医生后予以出院。    出院诊断：川崎病 支气管肺炎 肝功能损害    出院带药：" \
#                        u"阿司匹林25mg/#×60＃ 25mg  bid  po 肝泰乐50mg/#×1瓶  50mg  tid  po        " \
#                        u"潘生丁25mg/#×1瓶  25mg bid p.o. 澳特斯*1瓶 sig：2.5ml tid po        " \
#                        u"头孢克肟*1盒 sig：1/2包 bid po"
#     
#     # 同样生成模板，只是这次标签都是unknown而已，请把reserve_unknown设成True保留unknown标签
#     # 训练过程中我把unknown标签都扔了
#     # unknown的产生可能是标注不对的数据，也可能是我自己程序的bug
#     
#     print("Test case 1:")
#     # 但是现阶段这样也可以正常运行，只是需要保留unknown标签
#     A = document.LabeledDocument(test_doc,keep_separator=True)
#     B = seqlabel.SeqLabel(A, reserve_unknown=True)
#     print(B.input)
#     
#     label, word = tagger.tag(B,result_format="dual")
#     
#     # 不同的格式产生的结果不同
#     # print(tagger.tag(B,result_format="bind"))
#     
#     # 生成报告
#     # 这个是根据标注还原文档报告的结构
#     # 现阶段这部分没有采用任何学习算法，是基于语法规则的
#     # 为什么可以这么做呢，原因有二，一是输入文件多以短语为主，
#     # 分割之后大部分词语的作用域有限(跨逗号修饰的情况很少，但这是接下来要解决的难点)
#     # 二是专业术语和特殊词语很多，它们的结构往往固定，比如血常规化验单后面的格式肯定是每一条
#     # 一个nz.+[符号]+数据/阴阳性/+- 这个没必要用机器学习。
#     C = ReportGenerator.Report()
#     C.set_input(word,label)
#     report_str = C.generate_report()
#     
#     print(C.phrase_list)
#     print(C.phrase_label_list)
#     print(report_str)
#     
#     
#     print("\nTest case 2:")
#     
#     # 下面测试一下不在训练集的内容
#     A = document.LabeledDocument(test_doc_unknown,keep_separator=True)
#     B = seqlabel.SeqLabel(A, reserve_unknown=True)
#     print(B.input)
#     
#     label, word = tagger.tag(B,result_format="dual")
#     print(tagger.tag_bind_result)
#     C = ReportGenerator.Report()
#     C.set_input(word,label)
#     report_str = C.generate_report()
#     
#     print(C.phrase_list)
#     print(C.phrase_label_list)
#     print(report_str)
#     
#     # 来最后让我们把结果写入到excel文件中:
#==============================================================================
