# -*- coding: utf-8 -*-

"""
快速查错器(deprecated)
运行即可
会在resource文件夹下生成查错结果(train_input_validate_result.xlsx)
"""

import re

#TODO: 重构改代码
#TODO: 修改正则的存储位置

pattern = u'[^,，\s、　\'’\n。\;；：:【】（）]+@{1}[a-z]+'
seperator = u'[,，\s、　\'’\n。\;；：:【】（）]+'
number_reg_str =u"(?:(?<!\d)(?:(?:\d+(?:\.\d+)?－)|[\<\>\-])?\d+(?:\.\d+)?(?:(?:[\*×](?:10)\^(?:-?(?!0\d)\d+)))?)"
unit_reg_str= u"(?:(?:(?<=[\d\s])[a-zA-z]*(?:(?:/[a-zA-z]+(?![a-zA-z]))|[a-zA-z]+))|(?:(?<=[\d\s])\%{1})|(?:(?<=[\d\s])℃))"
#item_reg_str= u"(?:(?:[↑↓]\s*)?"+number_reg_str+u"(?:\s*)"+unit_reg_str+")"
item_reg_str= u"(?:(?:[><]\s*)?"+number_reg_str+u"(?:\s*)"+unit_reg_str+")"

eng_reg_str = u"\w+[+-＋－%↑↓]{1,1}\w*"

number_pattern = re.compile(number_reg_str)
unit_pattern = re.compile(unit_reg_str)
item_pattern = re.compile(item_reg_str)
eng_pattern = re.compile(eng_reg_str)


def check_input(sentence, err_format, warning_format):
    # Compatiability Check
    if not isinstance(sentence, str):
        print("Error: Blank Line?")
        args_for_xlsx = [err_format, u"cannot read this line"]
        return args_for_xlsx, []
    p = re.compile(pattern)
    phrases = re.split(seperator, sentence)
    args_for_xlsx = []
    count_list = []
    for phrase in phrases:
        # Check each part
        if len(phrase.split('@')) > 2:
            # This is the wrong part
            args_for_xlsx.append(err_format)
            args_for_xlsx.append(phrase + ' ')
        else:
            match = p.search(phrase)
            if match is not None:
                if len(match.group()) > 10:
                    args_for_xlsx.append(warning_format)
                    args_for_xlsx.append(phrase + ' ')
                split_result = match.group().split('@')
                count_list.append((split_result[0], split_result[1]))
                # args_for_xlsx.append(match.group())
                pass
            else:
                if '@' in phrase:
                    # This also does not make any sense
                    args_for_xlsx.append(err_format)
                    args_for_xlsx.append(phrase + ' ')
                else:
                    # args_for_xlsx.append(phrase)
                    pass

    if len(args_for_xlsx) == 0:
        args_for_xlsx.append('No wrong label here')
    return args_for_xlsx, count_list


def extract_input(sentence):
    '''
    This will extract valid elements in a sentence and make them as
    an appropriate input for the learning program
    :param sentence:
    :return:
    '''
    if not isinstance(sentence, str):
        print("Error!")
        return [],[]
    p = re.compile(pattern)
    phrases = re.split(seperator, sentence)
    args_list = []
    labels_list=[]
    for phrase in phrases:
        # Check each part
        if len(phrase.split('@')) > 2:
            # This is the wrong part
            pass
        else:
            match = p.search(phrase)
            if match is not None:
                if len(match.group()) > 8:
                    pass
                else:
                    split_result = match.group().split('@')
                    args_list.append(split_result[0])
                    if split_result[1] in ['nz','n','nr']:
                        # 名词标注为1,(但是名词不代表就不存在修饰关系了，例如n.+v.+n.的结构，后一个名词也是前一个名词的修饰
                        # 这种情况需要依靠句法分析和句法特征辅助定位，现阶段先忽略
                        labels_list.append(1)
                    elif split_result[1] in ['a','d','q']:
                        # 修饰语标注为2
                        labels_list.append(2)
                    else:
                        # v. t. 等暂时算作无关连词
                        labels_list.append(0)
            else:
                if '@' in phrase:
                    pass
                else:
                    if len(phrase)>0:
                        if len(item_pattern.findall(phrase))>0:
                            #  数字加单位认为是个修饰语
                            labels_list.append(2)
                        elif len(eng_pattern.findall(phrase))>0:
                            #   英文术语标注为1,i.e. 名词
                            labels_list.append(1)
                        else:
                            # 未标记词(除了数值)都认为是不重要的无关词
                            labels_list.append(0)
                        args_list.append(phrase)
        args_list.append(u"分隔符")
        labels_list.append(0)

    return args_list,labels_list

if __name__=="__main__":
    import xlsxwriter
    import pandas as pd
    import os

    rootdir = os.path.dirname(__file__)
    filename = os.path.join(rootdir, "../resources/train_input.xls")
    sample = pd.read_excel(filename)

    pattern = u'[^,，\s、　\n.。;：]+@{1}[a-zA-z]+'
    seperator = u'[,，\s、　\n.。;：]'
    count_list=[]

    workbook = xlsxwriter.Workbook(os.path.join(rootdir, "../resources/train_input_validate_result.xlsx"))
    worksheet = workbook.add_worksheet()

    # Set up some formats to use.
    red = workbook.add_format({'color': 'red'})
    yellow = workbook.add_format({'color': 'blue'})

    index=1
    print('working!')
    count_list=[]
    print(len(sample[u"内容"]))
    for each in sample[u"内容"]:
        #print str(index)+':'
        str_args,valid_items= check_input(each,red,yellow)
        count_list= count_list+valid_items
        args=['A'+str(index+1)]+str_args
        worksheet.write_rich_string(*args)
        index= index+1
    workbook.close()

    from collections import Counter
    w=Counter(map(tuple, count_list)).items()
    w=sorted(w, key=lambda a: a[1])