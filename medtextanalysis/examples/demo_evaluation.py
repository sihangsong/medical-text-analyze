"""

目前只能做到评估序列标注的好坏，还没有想好应该如何按照准确率公式计算



"""

from medtextanalysis.util.evalutation import bio_classification_report
import medtextanalysis.algorithm.tagger as Tagger
import medtextanalysis.algorithm.trainer as Trainer
import medtextanalysis.core.document as document
import medtextanalysis.core.sequalized_label as seqlabel
import os
import pandas as pd


if __name__=="__main__":
    # 测试文本的地址
    rootdir = os.path.dirname(__file__)
    train_source = os.path.join(rootdir, "../resources/train_input.xls")
    # 模型输出地址
    out = os.path.join(rootdir, "demo_crfsuite_output")

    # 读取有标定批次的内容
    table = pd.read_excel(train_source)
    docs = table[table.标定批次 <= 5][u"内容"]

    # 确认测试用的数目
    print("We have %s item for evaluation." % len(docs))

    # 生成序列标注
    documents = []
    for each_line in docs:
        # 带标点的情形下清洗数据
        A = document.LabeledDocument(str(each_line), keep_separator=True)
        # 生成特征模板和标签
        B = seqlabel.SeqLabel(A)
        documents.append(B)

    # 生成模型，只需要运行一次
    test = Trainer.CRFTrainer()
    for each in documents:
        # 加入训练集
        test.append(each)
    # 训练
    test.train(out)

    # 生成训练标注:
    y_train= [X.label for X in documents]

    # 下面生成预测标签
    test_source = os.path.join(rootdir, "../resources/train_input.xls")
    table = pd.read_excel(train_source)
    docs = table[u"内容"][docs.index]

    documents = []
    for each_line in docs:
        # 带标点的情形下清洗数据
        A = document.LabeledDocument(str(each_line), keep_separator=True)
        # 生成特征模板和标签
        B = seqlabel.SeqLabel(A)
        documents.append(B)

    # 调用模型
    tagger = Tagger.CRFTagger()
    tagger.open(out)

    y_test = [tagger.tag(X) for X in documents]

    #准确率
    print(bio_classification_report(y_train, y_test))
