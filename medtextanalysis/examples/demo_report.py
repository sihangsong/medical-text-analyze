# -*- coding: utf-8 -*-
"""
一个demo脚本(deprecated)，演示整个工作流程
这是上一个版本的，最新版的请看同文件夹下的
demo_crfsuite.py
"""

import os.path as ospath
from functools import reduce

import numpy as np
import pandas as pd
from jieba import load_userdict
from medtextanalysis.sentence2report.relation_evaluator import RelationEvaluator
from medtextanalysis.sentence2report.word_label_predict import \
    get_classifier, get_predict_result

from medtextanalysis.data_processing.data_analyzer import get_train_test_pack
from sentence2report.word_vec import get_gensim_model


def form_result_str(cwords,mwords):
    ans= ""
    for i in range(len(cwords)):
        ans += "({} : {}); ".format(cwords[i],mwords[i])
    return ans


if __name__=="__main__":

    import logging

    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    rootdir = ospath.dirname(__file__)
    print(rootdir)
    rootdir= ospath.join(rootdir,'../resources')
    input_sentences = ospath.join(rootdir, "test_input.xls")
    input_bow= ospath.join(rootdir, 'bag_of_words.txt')
    input_userdict= ospath.join(rootdir, 'jieba_dict.txt')

    result_file_name = 'demo_result_with_comma.xlsx'

    # Step 0, build up the word vector

    print('working!')
    print('Get word vector from all data (include labeled and tested)')
    model = get_gensim_model(input_bow)

    index = 1

    print('Get training set and test set!')

    # Step 1, 分词

    # jieba的加载机制导致这个加载只是一次性的
    load_userdict(input_userdict)
    training_words,training_labels, test_words, test_labels = get_train_test_pack(input_sentences)

    training_words= reduce(lambda x,y: x+y, training_words)
    training_labels = reduce(lambda x, y: x + y, training_labels)
    print('We have %d training words and %d test words' % (len(training_words), len(reduce(lambda x, y: x + y, test_words))))

    X_train = np.array([model.wv[word] for word in training_words])
    Y_train = np.array(training_labels)

    print("First set up the classifier!")
    classifier= get_classifier(X_train,Y_train)
    print("Classifier Done!")
    centers=[]
    modifys=[]
    reports=[]
    scores=0

    print("Now we predict and formalize the final result")

    relation_tool =RelationEvaluator();

    for i in range(len(test_words)):
        test_word= test_words[i]
        test_label = test_labels[i]
        # to prevent the not finding word error
        for i in range(len(test_word)):
            try:
                model.wv[test_word[i]]
            except:
                test_word[i]=u'无'
        # replace vectors into word list

        X_test = np.array([model.wv[word] for word in test_word or model.wv['无']])
        Y_test = np.array(test_label)

        # Step 2, 中心语和修饰语分类学习
        predict_label,score= get_predict_result(classifier, X_test, Y_test)
        scores+=score
        #print(predict_label[0],score)

        # Step 3, 根据学习结果提取特征对

        center,modify = relation_tool.extract_pairs(test_word, predict_label)
        reports.append(form_result_str(center,modify))

    print('NuSVC (F1 score={:.3f})'.format(scores/len(test_words)))

    print('Finally, save all results.')
    sample = pd.read_excel(input_sentences)
    save = pd.DataFrame({'Origin': sample['内容'][500:], 'Report': reports})
    save.to_excel(result_file_name,index=True,encoding='utf-8',sheet_name='sample1')
    print('Done! Check {} now!'.format(result_file_name))