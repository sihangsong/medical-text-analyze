"""
演示如何使用data_cleaning 脚本

"""

import re

import medtextanalysis.core.document as document
import medtextanalysis.core.sequalized_label as seqlabel
import medtextanalysis.data_processing.data_cleaning as data_cleaning
from medtextanalysis.config import GlobalVar
from medtextanalysis.util.functional_wrapper import timer

# TODO: (wy) 把中间用到正则的部分放入data_cleaning脚本中

_doc_list = [
    u"【2011-05-07 08:56:00.0】患儿@n 体温@nz 平稳@a , 仍有@d 阵@a 咳@nz , 无@d 明显@a 气促气喘@nz , "
    "无@d 紫绀@nz , 胃纳@nz 可@a , 无@d 吐泻@nz , 大便@n 无殊@a 。    查体@nz ：神@nz 清@a , 气@nz 平@a"
    ", 反应@nz 可@a , 前囟@nz 平@a , 咽@nz 红@a , 双扁@nz 红@a I 度肿大@a , 双肺呼吸音@nz 粗@a , 可及@d "
    "痰鸣音@nz , 心律@nz 齐@a , 心音@nz 有力@a , 未及@d 杂音@nz . 腹@nz 软@a , 平坦@a  ,  神经系统@nz "
    "未见@d 阳性体征@nz 。    辅检@nz ：2011-05-06:细胞免疫@nz CD3+@a 65.00 ; CD4+@nz 48.00 ; CD8+@nz "
    "15.00 ; CD19+@nz 26.00 ; CD16+56+/CD45+@nz 6.00 ; CD4+/CD8+@nz  3.23 ; WBC@nz 11.80 ; Lymphocytes@nz "
    "6.47 ; CD3+@a 4.19 ; CD4+@nz 3.09 ; CD8+@nz  0.96 ; CD19+@nz 1.70 ; CD16+56+/CD45+@nz 0.37 ; 未见@d"
    "细胞紊乱@nz  ;           2011-05-05: 肺炎支原体抗体@nz 1：40阴性@a  ; TB@nz 阴性@a  ; 示无@d 支原体@nz"
    "及@c 结核杆菌@nz 感染@v  ;           2011-05-05:乙丙肝病毒@nz 乙肝表面抗原@nz 0.03IU/ML ; 乙肝表面抗体@nz"
    "720.17MIU/ML ; 乙肝E抗原@nz 0.52S/CO   ; 乙肝E抗体@nz 1.97S/CO   ; 乙肝核心抗体@nz 0.08S/CO   ;"
    "乙肝核心-IgM@nz 0.10S/CO   ; 丙肝病毒抗体@nz 0.04S/CO   ; 示@v 无@d 肝肾功能@nz 正常@a  ;  "
    "2011-05-05:血常规@nz WBC@nz  20.4*10^9/L ; HCG@nz 116g/L ; PLT@nz  425*10^9/L ; NE%@nz 49% ; CRP@nz "
    "70mg/L ;           2011-05-06:免疫(BNP)@nz  免疫球蛋白G@nz 19.60 ; 免疫球蛋白A@nz 0.55 ; 免疫球蛋白"
    "M@nz 0.67 ; 免疫球蛋白E@nz ↑ 78.7IU/mL ; 总补体@nz （CH50@nz ) 52.27u/ml ; 补体C3@nz 0.93g/L ; "
    "补体C4@nz ↑ 0.44g/L ; 循环免疫复合物@nz 29.29RU/mL ; 示@v 存在@v 免疫@nz 紊乱@a           "
    "2011-05-06:免疫@nz  艾滋病抗体@nz 阴性@a  ; 梅毒特异抗体检测（TPPA）@nz 阴性@a  ; 示@v 无@d 艾滋病@nz "
    "及@c 梅毒@nz 感染@v  ;     处理@n ：1.继续@v 目前@t 抗感染@a 及@c 对症支持@a 治疗@nz 。      "
    "    2.注意患儿@n 咳嗽@nz 症状变化@n 。",

    u"Lab：2009-12-28: ALT 15U/L;AST ↑ 47U/L;    处理：黄玉娟主治医师查看患儿，患儿体温已恢复正常，咳嗽减轻"
    u"，病情好转中，予以出院。    出院诊断：川崎病；窦性心动过缓；支气管肺炎    出院带药：肠溶阿司匹林×1瓶 "
    u"25mg  Bid  po；肝泰乐×1瓶  1粒  tid  po；潘生丁×1盒 25mg bid po；希刻劳×1盒 1包 bid po；敌咳×1瓶 "
    u"2ml tid po",

    u"嘉兴市@ns 中医院@nt 7－19 血常规@nz WBC@nz 17.44×10^9/L,N@nz %77,PLT@nz 151×10^9/L，Hb@nz 125g/L,"
    u"CRP@nz 35.7mg/L,腹部B超@nz 示@v “脐周@nz 及@c 右下@f 腹肠腔@nz 局限性扩张@a ，腹部@nz 及@c 多枚@q "
    u"淋巴结@nz ，最大@a 13×9mm”7－24 我院门诊@nt 血常规@nz WBC@nz 12.9×10^9/L,N@nz %83,PLT@nz 252×10^9/L，"
    u"Hb@nz 106g/L,CRP@nz 101mg/L",

    u"02-21: CD3+@nz  65.00 ; CD4+@nz 38.00 ; CD8+@nz  23.00 ; CD19+@nz 20.00 ; " 
    u"CD16+56+/CD45+@nz 11.00 ; CD4+/CD8+@nz  1.66 ; 细胞免疫@nz 基本@d 正常@a 　　" 
    u"02-21: 尿常规@nz KET@nz ++++@a  ; ERY@nz 0 ; WBC@nz 0 ; 考虑@v 通体@n ４＋ 与@p " 
    u"患儿@n 进食@v 少@d 有关@p 　　02-21: ALT@nz 11U/L ; AST@nz 15U/L ; TP@nz 60g/L ;" 
    u" ALB@nz 37g/L ; GLOB@nz 23g/L ; BUN@nz 3.5mmol/L ; Cr@nz 35umol/L ; NA@nz 139mmol/L ;" 
    u" K@nz 3.6mmol/L ; CL@nz 101mmol/L ; ASO@nz 38IU/ML ; 肝肾功能@nz 基本@d 正常@a 　　" 
    u"02-21: WBC@nz ↑ 18.1*10^9/L ; RBC@nz ↓ 3.8*10^12/L ; HCG@nz ↓ 101g/L ; PLT@nz 203*10^9/L" 
    u" ; NE%@nz ↑ 80% ; 异常细胞@nz  0% ; CRP@nz ↑ 74mg/L ;",

    u"【2009-12-28 09:13:00.0】 "
    u"患儿@n 体温@nz 正常@a ,  少@d 咳@nz ,  无@d 吐泻@nz , 胃纳@nz 差@an , 大便@nz 无殊@ad。   "
    u" P.E@nz ：神@nz 清@ad , 气@nz 平@ad , 左下颌@nz 有肿胀@nz , 可@d 触及@v 数枚@q 肿大@an 淋巴结@nz ,"
    u" 双眼球结膜@nz 已无明显充血@a , 口唇@nz 暗红@an 有@d 皲裂@an , 舌乳头@an 增粗@a 较前减轻@d  , 咽部@nz "
    u"红肿@an , 双肺呼吸音@nz 粗@ad , 未及@d 罗音@nz , 心音@nz 有力@ad , 律@nz 齐@ad , 无@d 杂音@nz , "
    u"腹@nz 软@an , 肝脾@nz 未及@d 肿大@an , 四肢活动@nz 可@a , 神经系统@nz 未见@d 异常@a 。肢端@nz 明显蜕皮@ad 。  "
    u"  Lab@nz :2009-12-27: ESR@nz  ↑ 56mm/h ; 提示@v 血沉@nz 较前降低@a    2009-12-27: WBC@nz  6.4*10^9/L ;"
    u" RBC@nz  4.08*10^12/L ; HCG@nz  110g/L ; MCV@nz  85fL ; PLT@nz  ↑ 597*10^9/L ; NE%@nz  ↓ 45% ; LY%@nz  "
    u"41% ; CRP@nz 1mg/L ; 血象@nz 较前@d 好转@a    2009-12-25: 24Holter@nz 最快心率@nz 160 , 最慢心率@nz 57次 , "
    u"平均心率@nz 95次 ,  房性早搏@nz 3次/24小时 , SDNN@nz 128    处理@n :继续观察@v 患儿@a 病情变化@n. "


]


@timer
def check_robustness(filename):
    """

    :param filename:
    :return:
    """
    import pandas as pd
    read_content = pd.read_excel(filename)

    count= 0
    for each in read_content[u"内容"]:
        count += 1
        if count>=50:
            break
        try:
            A = document.LabeledDocument(each)
            B = seqlabel.SeqLabel(A)
            print(A.sentences)
            print(A.label_from_data_list)
        except:
            print("Line %s cause an error" % str(count))
        finally:
            # print("Line %s is ok" % str(count))
            pass



if __name__=="__main__":
    # 演示一个数据清洗的流程
    # for each in _doc_list:
    #
    #     answer, dict = do_cleaning(each)
    #     for sth in answer:
    #         sth, labels = data_cleaning.extract_data_labels(sth)
    #         print(labels)
    #         print(sth)
    #     print('\n')
    from functools import reduce

    # 演示Document类的处理
    for each in _doc_list[4:]:
        A = document.LabeledDocument(each,keep_separator=True)
        #print(A.sentence_list)
        B = seqlabel.SeqLabel(A)
        print([(a,b) for a,b in zip(reduce(lambda x, y: x + y, A.sentences),reduce(lambda x, y: x + y, A.label_from_data_list))])
        print(B.model)
        print(B.label)




    # import os.path as ospath
    # rootdir = ospath.dirname(__file__)
    # rootdir = ospath.join(rootdir,'../resources')
    # input_sentences = ospath.join(rootdir, "test_input.xls")

    # 测试数据会不会造成unexpected error
    # check_robustness(input_sentences)
