"""
检查文件的格式

"""

from pycrfsuite import Tagger
import re
from .funcs import strQ2B,force_decode

def check_crfmodel_validity(filename):
    """
    检查crf model 是否合法的最简单粗暴的办法
    就是打开一次看看
    注意打不开和打开了格式错误是不一样的错误
    :param filename:
    :return:
    """
    try:
        tag= Tagger()
        tag.open(filename)
    except ValueError as e:
        return "Format Error"
    except IOError as e:
        return "IO Error"
    except FileNotFoundError as e:
        return "FileNotFound Error"
    except Exception as e:
        return "Unknown Error"

    return "OK"




def check_search_dict_validity(input):
    """
    搜索词典格式
    每行由若干个词语组成,每个词语之间用半角空格隔开,不允许词语内部出现空格
    第一个词语为中心词, 之后以单字词语'$'分割, 表示前后修饰的分割指示符, '$'也可以没有，表示全是前向修饰语
    对于需要提取的数字, 不要使用'$',一律使用{%d}(正数,这里会考虑汉字)或{%f}(浮点和科学计数法)代替
    (可扩展考虑)

    一个合法的格式为:
        发热 有 无 $ {%d}天

    格式非法的情况:
        某一行为空
        {}不匹配,嵌套,或者里面出现了非法的字符标识
        出现了一个以上的$,或者出现了一次的'$'不是单字，或者'$'出现在了第一个词

    注: 空文件是合法的！

    :param input: 读入的二进制流或者字符串
    :return:
    """
    if isinstance(input,bytes):
        input = force_decode(input)
    input = strQ2B(input)


    lines= input.splitlines()

    sp = re.compile(u"\s+")

    pattern = re.compile(u"(?<=\{%)[A-Za-z]+(?=\})")

    blank_line= False

    #print('\n')
    #print(lines)

    for each_line in lines:
        each_line = sp.sub(" ", each_line)

        if len(each_line) == 0 or (len(each_line)==1 and each_line==" "):
            blank_line = True
        if len(each_line) > 0 and blank_line:
            return False
        cnt = 0
        # 检查特殊字符匹配
        for chr in each_line:
            if "{" == chr:
                cnt += 1
            elif "}" == chr:
                if cnt>1:
                    return False
                cnt -= 1
        if cnt != 0:
            return False

        # 检查{}内语法糖是否正确
        separate_found = False
        for (index,each) in enumerate(each_line.split(" ")):
            match = pattern.findall(each)
            for result in match:
                if result not in ["d","f"]:
                    return False
            if "$" in each:
                if len(each)>1 or separate_found:
                    return False
                if index==0:
                    return False
                separate_found = True

    return True


def check_merge_dict_validity(input):
    """
    合并词典的格式
    每行若干个输入，由空格分割，表示要合并的内容，
    每个输入中可以使用 半角 '&' 作为分隔符，表示多种情况，如：父亲&母亲
    一个合法的输入为:
        父亲&母亲 身高
        左&右&左右 心房 心室&其它&心率
    我们不会检查词语有没有意义，请注意这点(比如误输入"父亲母亲"是不会报错的)

    非法输入:
        出现空行
        & 单独出现
        & 前面或者后面没有接词语

    注: 空文件是合法的！

    :param input:读入的二进制流或者字符串
    :return:
    """
    if isinstance(input, bytes):
        input = force_decode(input)

    input = strQ2B(input)
    sp = re.compile(u"\s+")

    lines = input.splitlines()

    blank_line = False

    for each_line in lines:
        each_line = sp.sub(" ",each_line)
        if len(each_line) == 0 or (len(each_line)==1 and each_line==" "):
            blank_line = True
        if len(each_line) > 0 and blank_line:
            return False

        phrase_list= each_line.split(" ")
        for (index, each) in enumerate(phrase_list):
            if len(each) == 0:
                continue
            if each[0] == '&' or each[-1] == '&':
                return False
            if "@" in each:
                if index == len(phrase_list)-1:
                    if "@" in each[1:]:
                        return False
                else:
                    return False

    return True

def check_protect_dict_validity(input):
    """
    保护词典格式：
    输入每行两个用空格分开的词语，第一个为保护词（完全匹配）第二个未标记

    注意千万不要把例如 "天", "次数" 的单位单独列在保护词典里，这些通过少量标记很容易正确找到，不推荐正则，尤其是"天"这个字。
    中华汉语博大精深，太多词带这个字了这个筛选太不智能了，标记会比这个更管用。

    但是像 mg/kg/d*2d 这样的特殊单位，保护词典是有必要的，特别是如果某些语料中经常出现它们的话

    非法格式：

    开头有空格

    :param input:
    :return:
    """
    if isinstance(input, bytes):
        input = force_decode(input)

    input = strQ2B(input)
    sp = re.compile(u"\s+")

    lines = input.splitlines()

    blank_line = False

    for each_line in lines:
        each_line = sp.sub(" ", each_line)
        each_line = each_line.lstrip(" ").rstrip(" ")
        if len(each_line) == 0 or (len(each_line) == 1 and each_line == " "):
            blank_line = True
        if len(each_line) > 0 and blank_line:
            return False

        phrase_list = each_line.split(" ")
        if len(phrase_list) !=2:
            return False
        if not phrase_list[1].isalpha():
            return False
    return True