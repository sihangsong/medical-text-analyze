"""
有用的装饰器

装饰器是一种语法糖，它主要是为了优雅地写程序

这涉及到python的闭包，属于比较复杂的东西了，
所以不要写太过于复杂的装饰器不然维护的人不知道你在干什么

但是计时和打日志我还是喜欢写装饰器
"""

import functools
from time import time


def timer(func):
    """
    计时装饰器，用法

    @timer
    def some_function():
        do_sth

    some_function()
    程序运行时自动帮你计时，并在结束后打印信息

    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kw):
        tic = time()
        result = func(*args,**kw)
        toc = time()
        print ("%f seconds has passed" % (toc-tic))
        return result
    return wrapper
