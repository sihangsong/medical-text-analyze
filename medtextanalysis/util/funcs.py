# -*- coding: utf-8 -*-
"""
存放一些有用的全局小函数
"""


def strQ2B(ustring):
    """
    全角转半角函数
    :param ustring:
    :return:
    """
    ss = ""
    for s in ustring:
        rstring = ""
        for uchar in s:
            inside_code = ord(uchar)
            if inside_code == 12288:
                # 全角空格直接转换
                inside_code = 32
            elif 65281 <= inside_code <= 65374:
                # 全角字符（除空格）根据关系转化
                inside_code -= 65248
            rstring += chr(inside_code)
        ss+=rstring
    return ss

def force_decode(string, codecs=None):
    """

    :param string:
    :param codecs:
    :return:
    """
    if codecs is None:
        codecs = ["UTF-8", "GBK", "gb2012"]
    for i in codecs:
        try:
            return string.decode(i)
        except UnicodeDecodeError:
            pass
