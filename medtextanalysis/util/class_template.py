# -*- coding: utf-8 -*-

"""
这是一个类模板文件，用于开发时参考并合理使用Python中的类
开发过程中需要给类和类中所有方法写明注释文档
    \"\"\"Summary of class here.

    Longer class information....
    Longer class information....

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    \"\"\"

"""


class MyClass:
    def __init__(self, name1, age1):
        self.name = name1
        self.age = age1
# 因为会自动执行类中的 __init__方法，且该方法中有参数，所以实例化对象时需要传递参数
# self是一个形式参数, 当执行下句代码时，实例化对象obj1,那么self就等于obj1这个对象
obj1 = MyClass("IKTP", 22)
# 当执行下句代码时，实例化对象obj2,那么self就等于obj2
# 且这两个对象同都拥有两个属性：name,age
obj2 = MyClass("hahaha", 23)
# 当需要调用对象的属性时，即name和age属性，可以直接用对象名字后打点调用需要的属性，例如：
# print(obj1.name)  # 执行结果：IKTP
# print(obj1.age)  # 执行结果：22
# print(obj2.name)  # 执行结果：hahaha
# print(obj2.age)  # 执行结果：23


class MyClass:
    public_var = "this is public_var"

    def __init__(self, name1, age1):
        self.name = name1
        self.age = age1

    #  在类里面定义的函数，统称为方法，方法参数自定义，可在方法中实现相关的操作
    #  创建实例方法时，参数必须包括self，即必须有实例化对象才能引用该方法，引用时不需要传递self实参
    def speak(self):
        print("this is def speak.%s 说：我今年%d岁。" % (self.name, self.age))

    #  我们要写一个只在类中运行而不在实例中运行的方法. 如果我们想让方法不在实例中运行
    #  比如我们需要类中的基础字段public_var,根本不需要实例化对象就可以拿到该字段
    #  这时就需要装饰器@classmethod来创建类方法
    #  创建类方法时，参数必须包括cls，即必须用类来引用该方法，引用时不需要传递cls实参
    @classmethod
    def speak2(cls):
        print("this is classmethod")
        return cls.public_var

    #  经常有一些跟类有关系的功能但在运行时又不需要实例和类参与的情况下需要用到静态方法
    #  写在类里的方法,必须用类来调用(极少数情况下使用,一般都在全局里直接写函数了)
    @staticmethod
    def speak3(name2, age2):
        print("this is staticmethod.%s 说：我今年%d岁。" % (name2, age2))


# obj = MyClass("IKTP", 22)
# 无论是类方法、静态方法还是普通方法都可以被实例化的对象调用
# 但是静态方法和类方法可以不用对象进行调用
# obj.speak()  # 执行结果：this is def speak.IKTP 说：我今年22岁。
# var = obj.speak2()  # 执行结果：this is classmethod
# print(var)  # 执行结果： this is public_var
# obj.speak3("liu", 23)  # 执行结果：this is staticmethod.liu 说：我今年23岁。

# MyClass.speak()  # 报错，实例方法不能直接被调用，必须需要实例化的对象调用
# var2 = MyClass.speak2()  # 不需要实例化对象即可拿到该字段
# print(var2)  # 不需要实例化对象即可拿到该字段
# MyClass.speak3("abc", 12)  # 不需要实例化对象即可执行


class P:



    def __init__(self,x):
        self.x = x



    # python 中除了用__x表示private变量,_x表示protected变量
    # 还有个重要的property装饰器
    # 这个东西的功能在于优雅地封装get和set方法，从而使得调用变得容易
    # 请看下面示例如何简单地访问私有变量__x
    # 但是大部分情况不需要用这个，因为这个和一个public变量等价，只是更安全
    # 什么时候一定要用这个呢，答案是没有
    # 建议使用情形：
    #   1. 变量需要显式地进行格式检查
    #   2. 相互关联的变量，修改一个另一个要跟着变，一个public变量不够
    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, x):
        if x < 0:
            self.__x = 0
        elif x > 1000:
            self.__x = 1000
        else:
            self.__x = x

    @x.getter
    def x(self, x):
        return self.__x

    @x.deleter
    def x(self, x):
        del self.__x

# 使用样例，虽然类里的代码变长了，但是调用变量变得容易了
# obj = P(-1)
# print(obj.x)
# obj2 = P(100)
# print(obj.x+obj2.x)
# del obj.x