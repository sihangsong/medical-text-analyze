"""
命令行封装:



"""
import sys
import os
import argparse
import configparser
import logging

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
import medtextanalysis.examples.demo_crfsuite as dm

# 用于获取时间
import time

def run_train_tool():
    # 命令行中需要显示的软件说明
    APP_DESC = """
    --------------------------------------------------
    训练程序:

    基于CRF序列标注数据，训练模型文件，以利用模型文件预测未标记的数据的词语成分，进而
    实现关键词提取，关系抽取等实际应用问题。

    训练程序会读取训练用文本并生成模型。训练用文本应该是一个excel文件(.xls or .xlsx), 
    程序会寻找表头为”内容“的列作为输入。训练会读取第一个工作簿的所有文本，无论标记与否，
    请检查数据! 程序将会返回一个后缀为训练文件名+".crfmodel"的二进制文件。多个重名文件将以
    生成时间    在命名上加以区别([时间]_名字.crfmodel)。此外，会输出一个日志文件，
    文件为CRF模型文件名在加上 +".log"后缀

    预测程序会读取文件中每一行的信息，并且生成对应的预测结果。预测是需要有两个输入，第一个
    输入为模型文件的名称或者绝对路径，是训练得到的文件，而第二个参数是需要预测的内容文件。
    预测用文件也是一个excel文件，以表头"内容"的列作为输入。预测时会忽略数据的所有标记。
    预测成功的时候，程序会生成一个excel文件，其第一列为序号，第二列为原始输入，第三列为
    预测的结构化字符串。除此以外，算法可以指定一些辅助文件帮助训练。现阶段提供搜索词典，
    合并词典和保护词典，它们的格式和作用请参考文档。

    程序的具体用法请输入 -h 参数查询
    --------------------------------------------------
        """
    APP_EPI = """
    --------------------------------------------------
    以下代码可能对您有帮助：
    python MTA_train_tool.py --train [sourceAdd]
    OR
    python MTA_train_tool.py --predict [modelAdd] [sourceAdd]
    python MTA_train_tool.py --predict [modelAdd] [sourceAdd] -s [searchDict] -m [mergeDict] -p [protectDict]
    OR
    python MTA_train_tool.py --config check
    python MTA_train_tool.py --config [paraName] [paraValue]
    --------------------------------------------------
        """
    # 修改或完成以下的部分
    parser = argparse.ArgumentParser(description=APP_DESC,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=APP_EPI)
    group = parser.add_mutually_exclusive_group()

    group.add_argument("--train", help="一个参数：语料输入文件的绝对地址")
    group.add_argument("--predict", nargs=2, help="两个参数：模型文件地址与目标文件地址", default="0")
    group.add_argument("--config", nargs="+" , help="check或输入三个参数：参数组名，要修改的参数名与参数值")
    parser.add_argument("-s", "--search_dict", help="一个参数：读入搜索字典的文件位置", default=None)
    parser.add_argument("-m", "--merge_dict", help="一个参数：读入合并字典的文件位置", default=None)
    parser.add_argument("-p", "--protect_dict", help="一个参数：读入保护字典的文件位置", default=None)
    args = parser.parse_args()
    temp = sys.stdout  # 记录当前输出指向，默认是consle
    if not args.train and not len(args.predict) == 2 and not args.config:  # FLAG
        print("""
    没有输入参数或参数数量错误，请用-h指令获取帮助      
    以下代码可能对您有帮助：
        python MTA_train_tool.py --train [sourceAdd]
    OR
        python MTA_train_tool.py --predict [modelAdd] [sourceAdd]
        python MTA_train_tool.py --config check
        python MTA_train_tool.py --config [paraName] [paraValue]
                  """)
    elif args.train:
        if args.search_dict or args.merge_dict or args.protect_dict:
            print("参数输入冗余，请检查后重新输入")
        else:
            if isinstance(args.train,list):
                args.train=args.train[0]
            add = os.path.split(args.train)[0]
            file_lname = os.path.split(args.train)[1]
            file_sname = os.path.splitext(file_lname)[0]
    
            if add == "":
                #add = os.path.abspath(os.path.dirname(__file__))
                add = os.getcwd()
                train_filename = os.path.join(add, args.train)
            else:
                train_filename = args.train
            # 日志写之前判断是否可以读更好
            out = os.path.join(add, file_sname + ".log")
            tmpout = out
            fir = ""
            if os.path.exists(tmpout):
                fir = time.strftime("_%Y-%m-%d-%H%M%S", time.localtime())
                tmpout = os.path.join(add, file_sname + fir + ".log")
    
            with open(tmpout, "a+") as f:
                print("数据开始训练，该过程需要一定时间..")
                print("请耐心等候......")
                sys.stdout = f  # 输出指向txt文件
                print("-----------------------",
                      "\nfilepath:", add,
                      "\nfilename:", file_lname,
                      "\ntime:", time.asctime(),  # (ssh)已加上时间戳
                      "\n-----------------------")
                dm.train_data(train_filename, args.protect_dict, fir)
                sys.stdout = temp
                print("训练完成，模型文件与日志已输出")


    elif args.predict and not args.predict[0]=="0":
        add = os.path.split(args.predict[0])[0]  # 模型文件地址
        file_lname = os.path.split(args.predict[0])[1]
        file_sname = os.path.splitext(file_lname)[0]

        if add == "":
            #add = os.path.abspath(os.path.dirname(__file__))
            add = os.getcwd()
            model_input_filename = os.path.join(add, args.predict[0])
        else:
            model_input_filename = args.predict[0]

        add = os.path.split(args.predict[1])[0]  # 模型文件地址
        file_lname = os.path.split(args.predict[1])[1]
        file_sname = os.path.splitext(file_lname)[0]

        if add == "":
            #add = os.path.abspath(os.path.dirname(__file__))
            add = os.getcwd()
            predict_input_filename = os.path.join(add, args.predict[1])
        else:
            predict_input_filename = args.predict[1]

        out = os.path.join(add, file_sname + ".log")
        tmpout = out
        fir = ""
        if os.path.exists(tmpout):
            fir = time.strftime("_%Y-%m-%d-%H%M%S", time.localtime())
            tmpout = os.path.join(add, file_sname + fir + ".log")
        with open(tmpout, "a+") as f:
            print("数据开始处理，该过程需要一定时间..")
            print("请耐心等候......")
            sys.stdout = f  # 输出指向txt文件
            print("-----------------------",
                  "\nfilepath:", add,
                  "\nfilename:", file_lname,
                  "\ntime:", time.asctime(),
                  "\n-----------------------")
            if args.search_dict:
                print("search_dict:", args.search_dict)
            if args.merge_dict:
                print("merge_dict:", args.merge_dict)
            if args.protect_dict:
                print("protect_dict:", args.protect_dict)
            dm.predict_data(model_input_filename, predict_input_filename, args.search_dict, args.merge_dict, args.protect_dict, fir)
            sys.stdout = temp
            print("处理完成，结构化文件与日志已输出")

            # Logging 生成日志最简单的解决方法: 将print的信息输出到文件中
            # 注意需求中log文件的命名要求
            
    elif args.config:
        config = configparser.ConfigParser()                #config 接口
        config.read_file(open("config.ini"))
        if args.config[0]=="check":                         #查看、打印参数功能
            print("\n训练器的参数如下")
            for sections in config.sections():
                print("\n",sections,"\n")
                for items in config.items(sections):
                    print(items)
        elif args.config[0] in config.sections():
            try:
                config.get(args.config[0],args.config[1])
            except:
                print("您所输入的参数不在",args.config[0],"参数列表中！")
                print(args.config[0],"中的参数有：")
                for items in config.items(args.config[0]):
                    print(items)
            else:                    #修改参数功能
                try:
                    config.set(args.config[0], args.config[1] , args.config[2])
                    config.getfloat("train","c1")
                    config.getfloat("train","c2")
                    config.getint("train","max_iterations")
                    config.getboolean("train","feature.possible_transitions")
                    config.getfloat("train","feature.minfreq")
                    config.get("predict","para1")
                    config.get("predict","para2")
                    config.get("predict","para3")                               #【重要】在增加参数时，这里的函数要根据参数类型填写！
                except:
                    print("输入的参数数量不对或不符合规则，规则如下:",
                          "\nc1:浮点数",
                          "\nc2:浮点数",
                          "\nmax_iterations:整数",
                          "\nfeature.possible_transitions：True或Flase",
                          "\nfeature.minfreq:浮点数",
                          "\npara1:***",
                          "\npara2:***",
                          "\npara3:***")                                       #【重要】在增加参数时，这里的规则要手动填写！
                else:
                    config.write(open('config.ini',"w"))
                    print(args.config[0],"的参数",args.config[1],"已成功修改为",args.config[2])                
        
        else:
            print("您所输入的参数组有误!")
            print("现有的参数组为：")
            for items in config.sections():
                print(items)
            print("您可以使用check命令查看参数")
            
            
    # group.add_argument(predict_parser)
# 以下实现命令行封装
if __name__=="__main__":
    run_train_tool()

